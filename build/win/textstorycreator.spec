# -*- mode: python -*-

from kivy.deps import sdl2, glew
import os

block_cipher = None

baseDir = os.path.normpath('../../')

def get_files(directory):
    result = []
    for root, directories, filenames in os.walk(os.path.join(baseDir, directory)):
        path = root[root.find(directory):]
        for filename in filenames:
            #print(os.path.join(path,filename))
            target = os.path.join(path, filename)
            source = os.path.join(root, filename)
            type = 'DATA'
            result.append((target, source, type))
    return result

def get_file(relative_path):
    target = os.path.normpath(relative_path)
    source = os.path.join(baseDir, relative_path)
    type = 'DATA'
    return (target, source, type)

docfiles = get_files('documentation')
img = get_files('img')
locale = get_files('locale')
garden_filebrowser = get_files('garden_filebrowser')
textstory_converter = get_files('textstory_converter')

base = [
    get_file('textstorycreator.kv'),
    get_file('localisation.py'),
    get_file('setupdefs.py'),
    get_file('textstorycreator.py'),
    get_file('requirements.txt'),
    get_file('requirements-win.txt'),
    get_file('LICENSE'),
    get_file('README.md')
]

a = Analysis(['..\\..\\textstorycreator.py'],
             pathex=['E:\\Programmieren\\textstory_creator\\build\\win'],
             binaries=[],
             datas=[],
             hiddenimports=['win32timezone'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='textstorycreator',
          debug=False,
          strip=False,
          upx=True,
          console=False , 
          icon='..\\..\\img\\app_icon.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               docfiles,
               img,
               locale,
               base,
               textstory_converter,
               garden_filebrowser,
               *[Tree(p) for p in (sdl2.dep_bins + glew.dep_bins)],
               strip=False,
               upx=True,
               name='textstorycreator')
