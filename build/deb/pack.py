# Copyright (c) 2018 Josa Wode. See LICENSE file for details.

"""
Script to copy required project files (and leaving out the trash).
"""

import sys
import os
import logging
from distutils import dir_util, file_util


logging.basicConfig(
    format='%(asctime)s:%(msecs)05.1f  %(levelname)s: %(message)s',
    datefmt='%H:%M:%S')
log = logging.getLogger()
log.setLevel(logging.INFO)


baseDir = os.path.normpath('../../')


type_excludes = ['.pyc', '__pycache__', '.git']
excludes = ['build', 'dist', 'settings.ini', '.gitignore', '.gitmodules']


def is_excluded(path):
    if path in excludes:
        return True
    for suffix in type_excludes:
        if path.endswith(suffix):
            return True
    return False


#walk through directory only as deep as given level (0: no subdirectories)
def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]


def copy(source, dest):
    log.info("\ncopying from %s to %s" % (source, dest, ))
    for root, directories, filenames in walklevel(source, 0):#os.walk(source):
        for filename in filenames:
            src_file_path = os.path.join(source, filename)
            if not is_excluded(src_file_path):
                log.info("copying " + filename)
                file_util.copy_file(src_file_path, os.path.join(dest, filename))
        for directory in directories:
            src_dir_path = os.path.join(source, directory)
            if not is_excluded(src_dir_path):
                dest_dir_path = os.path.join(dest, directory)
                dir_util.mkpath(dest_dir_path)
                copy(src_dir_path, dest_dir_path)


def main():
    if len(sys.argv) != 3:
        sys.exit("Wrong number of arguments, required source and dest")
    source = os.path.normpath(sys.argv[1])
    dest = os.path.normpath(sys.argv[2])
    global excludes
    for i in range(len(excludes)):
        excludes[i] = os.path.join(source, excludes[i])
    log.info("prepared excludes: " + str(excludes))
    copy(source, dest)


if __name__ == "__main__":
    main()
