![Textstory Creator](img/app_icon.png)

# Textstory Creator

Grafische Benutzeroberfläche für [textstory-to-beautiful-latex-html](https://github.com/jgehrcke/textstory)

Ein Projekt zur Konversion eines Dokuments, geschrieben in einem simplen Markup-Format, zu HTML- und LaTeX-Dokumenten -- optimiert für Lesbarkeit, mit besten typographischen Absichten.

Für den schnellsten Einstieg wirf einen Blick in das [Tutorial](https://josawode.de/texts/tutorial-textstorycreator/).

**Graphic user interface for [textstory-to-beautiful-latex-html](https://github.com/jgehrcke/textstory)**

**Document conversion from a simple markup format to HTML and LaTeX documents -- optimised for readability with best typographical intentions.**

## Contact

* web: [coding.josawode.de](https://coding.josawode.de)
* email: [coding@josawode.de](mailto:coding@josawode.de)

Please contact me with problems, suggestions, wishes, questions or anything else -- except spam, attacks, insults ;).

## Credits

* [Python](https://www.python.org/)
* [Kivy](https://kivy.org/)
* [Jan-Philip Gehrcke](https://gehrcke.de/)
* [TeX Live](https://www.tug.org/texlive/)
* [LaTeX / LuaTeX](http://www.luatex.org/)
* [KOMA-Script](https://www.ctan.org/pkg/koma-script?lang=en)
* [Microtype](https://www.ctan.org/pkg/microtype?lang=en)
* [Libertine](https://en.wikipedia.org/wiki/Linux_Libertine)
* [csquotes](https://www.ctan.org/pkg/csquotes?lang=en)
* [Tufte CSS](https://github.com/daveliepmann/tufte-css)
* [normalize.css](https://github.com/necolas/normalize.css)
* [HTML5 Boilerplate](https://html5boilerplate.com/)
* [Bembo](https://de.wikipedia.org/wiki/Bembo)
* [Markdown](https://daringfireball.net/projects/markdown/syntax)
* [TOML](https://github.com/toml-lang/toml)
* [Alexander Taylor](http://inclem.net/pages/kivy-crash-course/)
* [Sergey Cheparev](http://cheparev.com/kivy-receipt-custom-settings/)
* [Fossasia](https://blog.fossasia.org/awesome-kivy-revelations/)
* [Kivy Garden](https://github.com/kivy-garden/garden.filebrowser)

# Dokumentation

**English version [below](#markdown-header-english-documentation)**

## Überblick

Das Ziel dieses Projekts ist es, professionelle Textsetzung für den Bereich des kreativen Schreibens möglichst leicht zugänglich zu machen.
Dabei liegt der Fokus auf der Erstellung eines hochwertigen Druckerzeugnisses, doch auch für die digitale Welt wird ein ansprechendes Dokument erzeugt.

### LaTeX

[LaTeX](https://de.wikipedia.org/wiki/LaTeX) bietet vielfältige, hervorragende Möglichkeiten zur Dokumentenerstellung (nicht nur) für den Printbereich. 
Aus den erzeugten *LaTeX*-Dokumenten lassen sich problemlos *pdf*-Dokumente erstellen.
Aufgrund der historischen Entwicklung und des großen Funktionsumfangs ist die Verwendung von *LaTeX* jedoch kompliziert.
*Textstory Creator* konzentriert sich auf das Erstellen von Dokumenten für Geschichten (Kurzgeschichten, Romane, etc.) und bietet für diesen Anwendungsbereich eine einfache Möglichkeit zur Benutzung des mächtigen Werkzeugs *LaTeX*.

### HTML

Heutzutage ist die digitale Welt kaum aus unserem Leben wegzudenken. Über das Internet können einfach und günstig Inhalte für andere zugänglich gemacht werden.
Daher erzeugt der *Textstory Creator* auch eine *HTML*-Variante des Textes, die besonderes Augenmerk auf Skalierbarkeit legt, damit die Geschichte auf verschiedensten Geräten optimal gelesen werden kann.

### Markup

Zur Gestaltung des Textes (Kapitel, kursiver Textabschnitt, etc.) wird die Geschichte (das zu konvertierende *Textstory*-Dokument) mit einer einfachen *Markupsprache* formatiert. Diese orientiert sich sehr stark an [Markdown](https://daringfireball.net/projects/markdown/syntax), welches aufgrund seiner Einfachheit breite Verwendung in den Bereichen Web und Programmierung (Dokumentation) findet.

[Detaillierte Beschreibung](#markdown-header-textstory-markup)

Ich hoffe, in Zukunft die Zeit zu finden, einen WYSIWYM-Editor für eine noch einfachere Textgestaltung zu entwickeln.

### Einstellungen

Für verschiedene Angaben zum Dokument und der Konfiguration des gewünschten Erscheinungsbildes können im *Textstory Creator* diverse Einstellungen vorgenommen werden.

[Detaillierte Beschreibung](#markdown-header-einstellungen-vornehmen)

## Installation

Es stehen folgende Installer bereit:

* [Deb Paket](https://bitbucket.org/ausguss/textstory_creator/downloads/textstorycreator1.1.deb)
* [Windows Installer](https://bitbucket.org/ausguss/textstory_creator/downloads/TextstoryCreatorSetup1.1_win7_x86.exe)

Alternativ kann der *Textstory Creator* [manuell installiert](#markdown-header-manuelle-installation) werden.

Für die Pdf-Erstellung wird [TeX Live](https://www.tug.org/texlive/) vorausgesetzt:

[Installation von TeX Live](#markdown-header-installation-von-tex-live)

### Installation unter Ubuntu und anderen Debian-basierten Systemen

Installation über das Terminal mit `sudo apt install ./textstorycreator1.0.deb`.

Die Installation kann nicht über das *Software Center* durchgeführt werden.

*Installation getestet unter Ubuntu 16.04 64Bit*

*ACHTUNG: für Ubuntu 18.04 steht derzeit noch kein Kivy zur Verfügung, daher kann der Textstory Creator hier nicht verwendet werden.*

#### Weitere Hinweise

* Ubuntu: Terminal öffnen mit `STRG + ALT + T` oder Rechtsklick im Dateibrowser und Auswahl von *In Terminal öffnen*
* Alle Fragen während der Installation durch Eingabe von `y` bejahen, um den *Textstory Creator* und die benötigten Voraussetzungen zu installieren.
* Die Installation von Kivy wird in einem neuen Terminalfenster gestartet. Dort ebenfalls mit `y` bestätigen.
* Deinstallation über `sudo apt remove textstorycreator`

### Installation unter Windows

Nach dem Herunterladen des Installers kann dieser mit Doppelklick gestartet werden. Der Installer führt durch die weitere Installation.

*Installation getestet unter Windows 7 64Bit*

## Manuelle Installation

Klonen des Projekts einschließlich des erforderlichen Submoduls *textstory_converter*:

`git clone --recurse-submodules https://bitbucket.org/ausguss/textstory_creator.git`

*textstory_converter* Submodul: [textstory-to-beautiful-latex-html](https://github.com/jgehrcke/textstory)

Natürlich muss [Python](https://www.python.org/) installiert sein.

Es sollte die neueste Version von *pip* und *wheel* installiert sein:

`python -m pip install --upgrade pip wheel setuptools`

Unter Linux sollte den `pip install`-Befehlen `sudo -H` vorangestellt werden, damit Dateien in das Home-Verzeichnis heruntergeladen werden können.

### Voraussetzungen:

#### Windows

* docutils
* pygments
* pypiwin32
* kivy.deps.sdl2
* kivy.deps.glew (ab Python 3.5+ kann auch das Angle Backend verwendet werden: kivy.deps.angle)
* kivy.deps.gstreamer
* kivy
* [Python TOML](https://pypi.python.org/pypi/toml)

Vor der ersten Ausführung sind diese zu installieren:  
`pip install -r requirements-win.txt`

#### Linux

**Kivy** muss installiert sein: 
[Kivy Installationsanweisungen Linux](https://kivy.org/docs/installation/installation-linux.html)

**Weitere Anforderungen**:

* docutils
* pygments
* [Python TOML](https://pypi.python.org/pypi/toml)

Vor der ersten Ausführung sind diese zu installieren:  
`pip install -r requirements.txt`

#### Sonstige

[Installationsanweisungen der *Kivy*-Webseite](https://kivy.org/#download) für verschiedene Plattformen.

Installation der weiteren Anforderungen:
`pip install -r requirements.txt`

## Installation von TeX Live

### Debian

Falls noch nicht vorhanden kann *TeX Live* mit folgendem Befehl auf der Kommandozeile nachinstalliert werden:

`sudo apt-get install texlive`

### Windows

Installation über den [Installer](http://mirror.ctan.org/systems/texlive/tlnet/install-tl-windows.exe)

### Sonstige

Hinweise für die *TeX Live* Installation auf verschiedenen Systemen finden sich auf der [TeX Live Webseite](http://tug.org/texlive/).

## Verwendung des Textstory Creator

* [Sprache auswählen](#markdown-header-sprache-auswählen)
* [Pfade festlegen](#markdown-header-pfade-festlegen)
* [Einstellungen vornehmen](#markdown-header-einstellungen-vornehmen)
* [Konvertierung starten](#markdown-header-konvertierung-starten)

## Sprache auswählen

In der rechten oberen Ecke der Anwendung findet sich die Spracheinstellung. Hier kann aus den verfügbaren Übersetzungen die gewünschte ausgewählt werden. Diese Einstellung wird gespeichert und bleibt somit bei erneutem Start der Anwendung erhalten.

![Spracheinstellung](documentation/img/language-selection.png)

## Pfade festlegen

![Pfadauswahl](documentation/img/de/paths-tab.png)

Für die Konvertierung einer *Textstory* werden drei Pfadangaben benötigt:

+ Das *Ausgabeverzeichnis* bestimmt den Ort, an den bei der Konvertierung geschrieben wird.
+ In der *Setupdatei* werden die gemachten *Textstory Einstellungen* gespeichert.
+ Die *Storydatei* enthält die zu konvertierende *Textstory*.

### Eingabemöglichkeiten

![Eingabemöglichkeiten](documentation/img/de/paths-tab-input.png)

Es gibt folgende Möglichkeiten, einen Pfad anzugeben:

1. Per Drag&Drop können Dateien oder Verzeichnisse auf das Eingabefeld gezogen werden.
2. Der Pfad kann direkt eingegeben werden.
3. Ein Dateibrowser kann geöffnet werden.

![Dateibrowser](documentation/img/de/paths-tab-filebrowser.png)

Falls der angegebene Pfad noch nicht existiert, erscheint ein Dialog, über den das Verzeichnis oder die Datei angelegt werden kann.

### Standardpfade

![Standardpfade](documentation/img/de/paths-tab-defaults.png)

Bei Angabe des *Ausgabeverzeichnisses* wird in diesem nach der *Setupdatei* *setup.toml* und der *Storydatei* *textstory.txt* gesucht. Falls vorhanden werden diese übernommen.

### Dateiformat der Dokumentenquelle

Erwartet wird eine Text-Datei in UTF-8-Kodierung.

### Ausgabe

#### HTML

Das Unterverzeichnis *html* des *Ausgabeverzeichnisses* enthält die *HTML*-Version der *Textstory*. Nach der Konvertierung kann diese durch Öffnen der *index.html* im Browser angesehen werden bzw. durch Kopieren auf einen Webserver im Internet zugänglich gemacht werden.

#### LaTeX

Das Unterverzeichnis *latex* des *Ausgabeverzeichnisses* enthält die *LaTeX*-Version der *Textstory*. Die Datei *latex-document.tex* kann mit *LuaTeX* in ein *pdf*-Dokument umgewandelt werden. Dies ist auch durch Anwählen der Option *pdf erstellen* im *Konvertieren*-Reiter *Textstory Creator* möglich (erzeugt *latex-document.pdf* im selben Verzeichnis).

## Einstellungen vornehmen

![Einstellungen](documentation/img/de/setup-tab.png)

Unter dem Reiter *Textstory Einstellungen* werden -- unterteilt in die Kategorien 
*Allgemein*, *LaTeX* und *Html* verschiedene Angaben zu den zu erzeugenden Dokumenten gemacht.
Diese reichen von allgemeinen Angaben wie Titel oder Autor\*in über Konfiguration der Titelzeilen zu Einstellungen des Dokumentenformats.

Manche dieser Einstellungen sind optional und können über das Kästchen auf der linken Seite an- und abgewählt werden.

Die hier gemachten Einstellungen werden unmittelbar in die bei der *Pfadauswahl* angegebene *Setupdatei* geschrieben. Achtung, wird eine bereits vorhandene *Setupdatei* geladen, gehen zuvor gemachte Änderungen verloren (es sei denn, sie wurden in eine andere *Setupdatei* gespeichert). Beim Anlegen einer neuen Datei werden die aktuellen Einstellungen übernommen.

### Allgemeine Einstellungen

*Titel* -- Titel des Textes

*Untertitel* (optional) -- Untertitel des Textes

*Autor\*in* -- Autor\*in des Textes

*Sprache* -- Sprachkürzel (z.B. *de*, *en*), siehe [Selfhtml -- Übersicht zu Sprachenkürzeln nach ISO 639-1](https://wiki.selfhtml.org/wiki/Sprachk%C3%BCrzel)

### LaTeX-Einstellungen

#### LaTeX Titel

*Titel* (optional) -- Titel des Textes, falls abweichend von Allgemein.Titel

*Untertitel* (optional) -- Untertitel des Textes, falls abweichend von Allgemein.Untertitel

*Autor\*in im Titel* (optional) -- *Ja*: Im Anfangstitel wird Autor\*in mit angegeben (nicht verfügbar bei Buchdruck)

*Schmutztitel* (optional) -- Schmutztitel, falls abweichend vom Titel; definiert das [Textkommando](#markdown-header-latex-textkommandos) *\\storyhalftitle*

*ISBN* (optional) -- ISBN dieses Buches; definiert das [Textkommando](#markdown-header-latex-textkommandos) *\\isbn*

#### LaTeX Buchdruck

*Buchdruck* -- Legt fest, ob das Dokument für den Buchdruck vorgesehen ist.

*Ja:*

+ LaTeX-Dokumenttyp ist *scrbook*
+ Titelei: Seiten aus *latex/bookPreliminaries* werden dem Text in alphanumerischer Reihenfolge vorangestellt
+ Anhang: Seiten aus *latex/bookAppendix* werden dem Text in alphanumerischer Reihenfolge angehängt

*Nein:*

+ LaTeX-Dokumenttyp ist *scrreprt*
+ Das Dokument beginnt mit Titel, Untertitel sowie ggf. Autor\*in (Option *Autor\*in im Titel*)

#### LaTeX Layout:

*Definition der Seitengröße* -- Die Seitengröße kann entweder als DIN-Format oder über Angabe von Breite und Höhe der Seite definiert werden. Dies kann hier umgeschaltet werden.

*Seitenformat* -- Definiert die Seitengröße über DIN-Formate (a0 - a6, b0 - b6)

*Seitenbreite* -- Die Breite der Seite mit Einheit (z.B. "210mm", "21.5cm")

*Seitenhöhe* -- Die Höhe der Seite mit Einheit (z.B. "210mm", "21.5cm")

*Bundversatz* (optional) -- Die Bundzugabe, also der Teil der Seite der in der Bindung verschwindet, mit Einheit (z.B. "15mm")

*Schriftgröße* -- Die Schriftgröße für normalen Text in pt (z.B. 11 oder 12.5). Überschriften etc. erscheinen entsprechend skaliert.

#### LaTeX Kopfzeile

Die Kopfzeile des Dokuments zeigt standardmäßig auf geraden Seiten den Titel der Geschichte (Textkommando *\\storytitle*) und auf ungeraden Seiten das Kapitel (Textkommando *\\storychapter*). Dies ist auf Wunsch konfigurierbar. Dafür stehen neben normalem Text verschiedene [Textkommandos](#markdown-header-latex-textkommandos) zur Verfügung.

*Kopfzeile links* (optional) -- Kopfzeile für Seiten mit gerader Seitenzahl

*Kopfzeile rechts* (optional) -- Kopfzeile für Seiten mit ungerader Seitenzahl

#### LaTeX Inhaltsverzeichnis

*Inhaltsverzeichnis* -- *Ja*: Es wird ein Inhaltsverzeichnis eingefügt

*Titel des Inhaltsverzeichnis* -- Die Überschrift für das Inhaltsverzeichnis kann hier angepasst werden.

*Inhaltsverzeichnis Seitenumbruch* -- *Ja*: Nach dem Inhaltsverzeichnis wird auf einer neuen Seite begonnen.

#### LaTeX Kapitel

*Kapitel Seitenumbruch* -- *Ja*: Neue Seite bei Kapitelanfang

*Kapitelkopfzeile verstecken* (optional) -- *Ja*: Keine Kopfzeile auf Seiten mit Kapitelanfang (Standard: entspricht *Kapitel Seitenumbruch*)

#### LaTeX PDF-Einstellungen

*Pdf-Thema* -- Kurzbeschreibung für PDF Metadaten

*Pdf-Schlüsselworte* -- Eine Reihe Schlüsselwörter für PDF Metadaten getrennt durch Kommata

*Hat farbige Links* (optional) -- Die Hypersetup-Option 'colorlinks' des Dokuments

*Url-Farbe* (optional) -- Die Hypersetup-Option 'urlcolor' des Dokuments

*Linkfarbe* (optional) -- Die Hypersetup-Option 'linkcolor' des Dokuments

### HTML-Einstellungen

#### HTML Titel

*Titel* (optional) -- Titel des Textes, falls abweichend von Allgemein.Titel

*Untertitel* (optional) -- Untertitel des Textes, falls abweichend von Allgemein.Untertitel

*Header-Titel* (optional) -- Text für die Browserkopfzeile (Standard: "*Titel* | *Autor\*in")

#### HTML Metaangaben

*Regionalangabe* -- Die Regionalangabe für den Text (z.B. *de_DE* oder *en_GB*) für die HTML-Metadaten (meta property og:locale)

*Metabeschreibung* -- Eine kurze Beschreibung für die HTML-Metadaten (meta description und meta property og:description)

*URL* -- URL (Adresse) der HTML-Version der Geschichte (meta property og:url)

*Site-Name* -- Name der Webseite, auf der der Text zu finden ist (meta property og:site_name)

*Vorschaubild* (optional) -- Adresse des Vorschaubildes, das gezeigt wird, wenn die (HTML-)Geschichte irgendwo (mit *Open Graph Protocol*-Unterstützung) gepostet wird (meta property og:image)

## Konvertierung starten

Nachdem alle Pfade angegeben und die gewünschten Einstellungen gemacht sind, können im *Konvertieren*-Reiter mit einem Klick auf *Konvertierung starten* die Ausgabedokumente generiert werden.

Wird die Option *pdf erstellen* angewählt, startet *Textstory Creator* zudem die Pdf-Erzeugung aus den zuvor generierten LaTeX-Dateien mittels *LuaTeX* (*TeX Live* muss dafür installiert sein). Der Konvertierungs Vorgang wird zwei mal ausgeführt, damit die Seitenzahlen im Inhaltsverzeichnis korrekt angegeben werden.

Im unteren Bereich (scrollbar!) können die Konsolenausgaben der Konvertierung und somit etwaige Probleme nachvollzogen werden.

![Konvertierung](documentation/img/de/converter-tab.png)

## Textstory Markup

### Absatztrennung
Markup: Zeilenumbruch

Beispiel: 
```
Dies ist der erste Absatz.
Hier beginnt der zweite Absatz.
```


### Sektionstrennung
Markup: Doppelter Zeilenumbruch bzw. Leerzeile

Beispiel:
`Hier steht der Text des ersten Abschnitts.

Hier beginnt eine neue Sektion.`


### Überschriften
Markup: Doppelraute am Zeilenanfang

Beispiel: `## Kapitel 1`

Eine derart markierte Zeile wird als Überschrift (und Kapitelanfang) interpretiert.


### Anführungszeichen
Markup: Einfache Double-Quote-Paare.

Beispiel: `Er sagte: "Oh, Kacke!"`

In der Ausgabe werden französische Anführungszeichen gesetzt.

Refs:

* <https://de.wikipedia.org/wiki/Guillemets>


### Bindestriche
Markup: Einfaches Minus.

Beispiel: `Damen- und Herrentoilette`

Wird unverändert in den HTML- und LaTeX-Code übertragen.

Refs:

* <https://de.wikipedia.org/wiki/Viertelgeviertstrich#Bindestrich-Minus>


### Gedankenstriche (en-dash)
Markup: Doppelminus.

Beispiel: `Glitzer, Filz, Garn und Knöpfe -- seine Küche sah aus als wäre ein Clown in ihr explodiert.`

HTML: wird übersetzt zu en-dash (`&ndash;`).
LaTeX: wird übersetzt zu `--`.

Refs:

* <https://de.wikipedia.org/wiki/Halbgeviertstrich>


### Gedankenstriche (em-dash)
Markup: Dreifaches Minus.

Beispiel: `Glitter, felt, yarn, and buttons---his kitchen looked as if a clown had exploded.`

HTML: wird übersetzt zu em-dash (`&mdash;`).
LaTeX: wird übersetzt zu `---`.

Refs:

* <https://de.wikipedia.org/wiki/Geviertstrich>


### Kursiv
Markup: Mit Unterstrichen (_) oder Asterisken (*) umgeben.

Beispiel: `Hier ist _etwas_ kursiv geschrieben. Hier *noch etwas*.`

Der markierte Textabschnitt wird kursiv dargestellt.

Refs:

* <https://de.wikipedia.org/wiki/Kursivschrift>


### Fett
Markup: Mit doppelten Unterstrichen (__) oder doppelten Asterisken (**) umgeben.

Beispiel: `Hier ist __etwas__ fett gedruckt. Hier **noch etwas**.`

Der markierte Textabschnitt wird fett dargestellt.

Refs:

* <https://de.wikipedia.org/wiki/Schriftschnitt>


### Auslassungspunkte
Markup: Drei normale Punkte.

Beispiel: `Das ist... ähm... doof.`

HTML: wird übersetzt zu `&hellip;`.
LaTeX: wird übersetzt zu `\dots`.

Refs:

* <https://de.wikipedia.org/wiki/Auslassungspunkte>


### Fußnoten
Markup: Eckige Klammern.

Beispiel: `Das ist das WortNachDemDieFußNoteKommt[Die Fußnote].`


### Bilder
Markup: \!\[altText\]\(Bildpfad "optionaler Titel"\)

Auf diese Weise können Bilder integriert werden, die am angegebenen Pfad liegen. Der optionale Titel wird zu einer Bildunterschrift. Der altText dient im HTML als Alternative, falls das Bild nicht dargestellt wird, und für Screen Reader. Im LaTeX wird diese Angabe ignoriert.

Beispiel: `![Alternativtext](/path/to/img.jpg "optional title")`

### Maskierung von Sonderzeichen
Markup: Backslash gefolgt vom Sonderzeichen ('`\Sonderzeichen`')

In HTML, LaTeX und dem hier verwendeten Markup sind bestimmte Symbole als Teil der Syntax reserviert. Sie müssen maskiert werden, um als das bloße Symbol interpretiert zu werden und als solches im generierten Text aufzutauchen.

Folgende Zeichen sind zu maskieren ('`\`' voranstellen):  
`\`   Backslash &ndash; das Maskierungszeichen selbst  
`*`   Asterisk  
`_`   Unterstrich  
`{}`  Geschweifte Klammern  
`[]`  Eckige Klammern  
`#`   Raute  
`"`   Anführungszeichen / Double-Quote  
`!`   Ausrufezeichen (nur nötig, wenn '[' folgt)  
`--`  Doppel-Minus  
`$`   Dollar  

Folgende Zeichen können normal verwendet werden und werden automatisch in HTML- bzw. LaTeX-Schreibweise übertragen:  
`&`   wird in HTML zu '`&amp;`', in LaTeX zu '`\&`'  
`<`   wird in HTML zu `&lt;`  
`>`   wird in HTML zu `&gt;`  
`%`   wird in LaTeX zu `\%`  
`~`   wird in LaTeX zu `\textasciitilde`  
`^`   wird in LaTeX zu `\textasciicircum`  

Beispiel: `Spitze <voll spitze> und eckige Klammern \[weil es so schön ist\].`

Refs:

* <https://www.w3.org/International/questions/qa-escapes.de#use>
* <https://www.namsu.de/Extra/strukturen/Sonderzeichen.html>

## LaTeX Textkommandos

Für die Verwendung in (zusätzlichen) LaTeX-Dokumenten (s.u.) und bei bestimmten Einstellungen (s.o.) stehen folgende Befehle (über den gewöhnlichen LaTeX-Befehlsumfang hinaus) zur Verfügung:

* `\storytitle`     gibt den Titel aus
* `\storysubtitle`  gibt den Untertitel aus
* `\storyhalftitle` gibt den Schmutztitel aus
* `\storyauthor`    gibt den Autor aus
* `\storychapter`   gibt das aktuelle Kapitel aus
* `\isbn`           gibt die ISBN aus
* `\printtitle`     gibt formatiert Titel, ggf. Untertitel und bei Setup-Option *Autor\*in im Titel* zusätzlich den Autor aus

## Vorlagen, Titelei, Anhang, Lizenzen

### LaTeX Lizenz

Unter dem Dateipfad `latex/license.tex` kann eine Lizenz (als gültiges LaTeX-Dokument) bereitgestellt werden. Fehlt diese Datei, wird keine Lizenz an das Dokument angefügt. 

Für die Lizenzen existieren Vorlagen im Verzeichnis `latex/templates/licenses`.

### LaTeX Titelei und Anhang

Wurde die Setup-Option `bookPrint` gewählt, können LaTeX-Dokumente für die Titelei und den Anhang des Buches in dafür vorgesehene Verzeichnisse eingefügt werden. 
Die Dateien im jeweiligen Verzeichnis werden in alphanumerischer Reihenfolge in das Dokument eingefügt.

Verzeichnis für die Titelei: `latex/bookPreliminaries`

Verzeichnis für den Anhang: `latex/bookAppendix`

Vorlagen finden sich unter `latex/templates`.

### HTML Lizenz

Unter dem Dateipfad `html/license.tpl.html` kann eine Lizenz (als gültiges HTML ohne header und body) eingefügt werden. Das dafür vorgesehene CSS-Stylesheet findet sich unter `html/css/license-styles.css`. 

Für Lizenz und zugehöriges CSS finden sich Templates im Verzeichnis `html/templates/licenses`.

# English Documentation

## Overview

This project targets giving easy access to professional typesetting for creative writers. The focus lies on producing beautiful documents for high-end print. Additionally an agreeable document for the digital world is created.

### LaTeX

[LaTeX](https://en.wikipedia.org/wiki/LaTeX) provides many excellent possibilities for creating documents (not only) for print. The generated *LaTeX* documents can easily be converted to *pdf*. 
On the downside the huge scope and historical evolution makes *LaTeX* complicated to use.
*Textstory Creator* focusses on creating documents for stories (short stories, novels, etc.) and provides in this field an easy-to-use interface to the powerful tool *LaTeX*.

### HTML

It is hard to imagine the digital world missing from modern life. The internet lets us share content with others in a simple and inexpensive way. For those reasons *Textstory Creator* also creates a *HTML* version of the given text that especially focusses on scalability to make your story show on all kinds of devices in an optimal way.

### Markup

To layout the text (e.g. mark chapters or italic passages) the story (that is the *textstory* document to convert) is styled with a simple *markup language*. This language is strongly based on [Markdown](https://daringfireball.net/projects/markdown/syntax) which for its simplicity has spreaded broadly in the fields web and programming (documentation).

[Detailed description](#markdown-header-textstory-markup_1)

I hope to find time at some point in the future to develop a WYSIWYM editor to make styling your text even easier.

### Settings

There are various possibilities to define and individualise the look of the output documents in *Textstory Creators* *settings tab*.

[Detailed description](#markdown-header-einstellungen-vornehmen)

## Installation

The following installers are provided:

* [Deb Package](https://bitbucket.org/ausguss/textstory_creator/downloads/textstorycreator1.1.deb)
* [Windows Installer](https://bitbucket.org/ausguss/textstory_creator/downloads/TextstoryCreatorSetup1.1_win7_x86.exe)

Alternatively *Textstory Creator* can be [installed manually](#markdown-header-manual-installation).

For pdf creation [TeX Live](https://www.tug.org/texlive/) is required:

[TeX Live Installation](#markdown-header-tex-live-installation)

### Installation on Ubuntu or other Debian based systems

Install in terminal with `sudo apt install ./textstorycreator1.0.deb`.

Do not install via *Software Center*!

*Installation tested on Ubuntu 16.04 64Bit*

*ATTENTION: At the moment there is no Kivy release for Ubuntu 18.04 so Textstory Creator cannot be run on this platform.*

#### Additional Directions

* Ubuntu: Open terminal by hitting `CTRL + ALT + T` or right-clicking in file browser and selecting *Open in Terminal*
* Answer installer always with `y` for *yes* to install *Textstory Creator* and all its requirements.
* Kivy installation will be started in new terminal window, there also hit `y` to allow installation.
* Uninstall command: `sudo apt remove textstorycreator`

### Installation on Windows

After downloading the installer it can by started by double-clicking. The installer will help you through the installation.

*Installation tested on Windows 7 64Bit*

## Manual Installation

Clone *textstory_creator* including submodule *textstory_converter*:

`git clone --recurse-submodules https://bitbucket.org/ausguss/textstory_creator.git`

*textstory_converter* submodule: [textstory-to-beautiful-latex-html](https://github.com/jgehrcke/textstory)

Of course [Python](https://www.python.org/) has to be installed.

The latest version of *pip* and *wheel* should be installed:

`python -m pip install --upgrade pip wheel setuptools`

On linux systems `pip install` commands should be called with preceeding `sudo -H` to allow downloading files into the home directory.

### Requirements:

#### Windows

* docutils
* pygments
* pypiwin32
* kivy.deps.sdl2
* kivy.deps.glew (ab Python 3.5+ kann auch das Angle Backend verwendet werden: kivy.deps.angle)
* kivy.deps.gstreamer
* kivy
* [Python TOML](https://pypi.python.org/pypi/toml)

Install requirements before first:  
`pip install -r requirements-win.txt`

#### Linux

**Kivy** has to be installed: 
[Kivy Installation Instructions Linux](https://kivy.org/docs/installation/installation-linux.html)

**Additional Requirements**:

* docutils
* pygments
* [Python TOML](https://pypi.python.org/pypi/toml)

Install requirements before first start:  
`pip install -r requirements.txt`

#### Other

[Installation Instructions on *Kivy* Webseite](https://kivy.org/#download) for various platforms.

Install requirements before first start:  
`pip install -r requirements.txt`

## TeX Live Installation

### Debian

If the *TeX Live* package is not already installed, this can be done in terminal with the following command:

`sudo apt-get install texlive`

### Windows

Installation with [Installer](http://mirror.ctan.org/systems/texlive/tlnet/install-tl-windows.exe)

### Other

On the [TeX Live Webseite](http://tug.org/texlive/) you find information about installing on various platforms.

## Usage of Textstory Creator

* [Select language](#markdown-header-language-selection)
* [Set paths](#markdown-header-path-setup)
* [make your settings](#markdown-header-settings)
* [start conversion](#markdown-header-conversion)

## Language Selection

In the top right corner of the application you can find language selection. Here you may choose between available translations. This setting will be saved for future use of *Textstory Creator*.

![Language Selection](documentation/img/language-selection.png)

## Path Setup

![Path selection](documentation/img/en/paths-tab.png)

For *Textstory* conversion three paths have to be set:

+ *output folder* defines the place where the conversion output will be written to.
+ *setup file* stores your *Textstory settings*.
+ *story file* contains the *Textstory* to convert.

### How to set a path

![How to set a path](documentation/img/en/paths-tab-input.png)

You can use one or more of the following methods to set your path:

1. Drag a file or folder into the application and drop it on the input field.
2. Type in the path directly.
3. Use the file browser.

![file browser](documentation/img/en/paths-tab-filebrowser.png)

Should the given path not exist already you can select in a dialog popup whether to create it or not.

### Default Paths

![Default paths](documentation/img/en/paths-tab-defaults.png)

When setting *output folder* the application will look into it for the *setup file* *setup.toml* and the *story file* *textstory.txt* and select those if available.

### File Format of Source Document

An UTF-8 encoded document is expected.

### Output

#### HTML

The *output folders* subdirectory *html* contains the *HTML* version of the *Textstory*. After conversion it can be displayed in a web browser by opening *index.html* or made available to the public by copying *html* folder to a web server.

#### LaTeX

The *output folders* subdirectory *latex* contains the *LaTeX* version of the *Textstory*. The file *latex-document.tex* can be converted to *pdf* with *LuaTeX*. This is also possible by selection the option *create pdf* in *Textstory Creators* *converter tab* (will create *latex-document.pdf* in the same directory).

## Settings

![Settings](documentation/img/en/setup-tab.png)

In the tab *Textstory Setup* several settings have to be made. The settings are divided into the categories *General*, *LaTeX* and *Html* and reach from title or author declarations to header configurations or setting up document size.

Some of those settings are optional and can be en-/disabled through the checkbox to their left.

Each change in settings will immediately be stored in the *setup file* selected. 
Note that loading a setup file will overwrite current settings in *Textstory Creator* (settings previously stored to another document will of course be preserved). When creating a new *setup file* the current settings will be saved to it.

### General Settings

*Title* -- The text's title

*Subtitle* (optional) -- The text's subtitle

*Author* -- The text's author

*Language* -- Language short code (e.g. *de*, *en*), have a look at [Wikipedia -- ISO 639-1](https://en.wikipedia.org/wiki/ISO_639-1)

### LaTeX Settings

#### LaTeX Title

*Title* (optional) -- The text's title, if different from general title

*Subtitle* (optional) -- The text's subtitle, if different from general subtitle

*Print author on title* (optional) -- *Yes*: At main title the author's name will be printed (not available for book print)

*Half title* (optional) -- Half title, if different from title; defines [Text Command](#markdown-header-latex-text-commands) *\\storyhalftitle*

*ISBN* (optional) -- The book's ISBN; defines [Text Command](#markdown-header-latex-text-commands) *\\isbn*

#### LaTeX Book Print

*Book Print* -- Defines if document is for book printing.

*Yes:*

+ *LaTeX* document type *scrbook*
+ Preliminary pages: Pages in *latex/bookPreliminaries* will be prepended to the text in alphanumerical order
+ Appendix: Pages in *latex/bookAppendix* will be appended to the text in alphanumerical order

*No:*

+ *LaTeX* document type *scrreprt*
+ Document beginns with title, subtitle and possibly author (option *Print author on title*)

#### LaTeX Layout

*Page size definition* -- Page size can be set as DIN format or through width and height. Here you can choose the preferred method.

*Page format* -- Defines page size by DIN format (a0 - a6, b0 - b6)

*Page width* -- Page width with unit (e.g. "210mm", "21.5cm")

*Page height* -- Page height with unit (e.g. "210mm", "21.5cm")

*Binding offset* (optional) -- Part of the page that is lost in binding with unit (e.g. "15mm")

*Font size* -- Size of the normal text font in pt (e.g. 11 or 12.5). Headers etc. will be scaled relatively.

#### LaTeX Header

The documents header by default shows the story's title on even pages ([Text Command](#markdown-header-latex-text-commands) *\\storytitle*) and the current chapter on odd pages ([Text Command](#markdown-header-latex-text-commands) *\\storychapter*). The header can be configured with text and the use of [Text Commands](#markdown-header-latex-text-commands).

*Header left* (optional) -- Header for pages with even page numbers

*Header right* (optional) -- Header for pages with odd page numbers

#### LaTeX Table of Contents

*Table of contents* -- *Yes*: A table of contents will be displayed.

*Contents title* -- The header text for table of contents

*Table of contents pagebreak* -- *Yes*: After table of contents a new page will be started.

#### LaTeX Chapter

*Chapter pagebreak* -- *Yes*: Begin new page when new chapter starts.

*Hide chapter header* (optional) -- *Yes*: No header will be printed on pages with new chapter begin (default: same as *Chapter pagebreak*)

#### LaTeX PDF Settings

*Pdf subject* -- Short description for pdf metadata

*Pdf keywords* -- Some keywords for pdf metadata separated by comma

*Has color links* (optional) -- Document's hypersetup option 'colorlinks'

*Url color* (optional) -- Document's hypersetup option 'urlcolor'

*Link color* (optional) -- Document's hypersetup option 'linkcolor'

### HTML Settings

#### HTML Title

*Title* (optional) -- The text's title, if different from general title

*Subtitle* (optional) -- The text's subtitle, if different from general subtitle

*Header Title* (optional) -- Title shown in web browser tab (default: "*title* | *author*")

#### HTML Metadata

*Locale* -- The text's locale (e.g. *de_DE* or *en_GB*) for HTML metadata (meta property og:locale)

*Meta description* -- A short description for HTML metadata (meta description and meta property og:description)

*URL* -- URL (adress) of the story's HTML version (meta property og:url)

*Site name* -- Name of the website hosting the text (meta property og:site_name)

*Preview image* (optional) -- URL (address) of preview image shown when the (HTML) story is posted somewhere with *Open Graph Protocol* support (meta property og:image)

## Conversion

When all paths are set and the desired settings are made, the output files can be generated in *Convert* tab by clicking *Start conversion*.

Selecting the option *create pdf* will also trigger pdf creation of the previously generated *LaTeX* files via *LuaTeX* (TeX Live has to be installed for this). Conversion will be executed twice to make sure that the page numbers in table of contents are displayed correctly.

In the lower area of the tab (scrollable!) the conversion's console log is viewed. This is especially useful for finding problems in the conversion process if any.

![Conversion](documentation/img/en/converter-tab.png)

## Textstory Markup

### New Paragraph
Markup: newline

Example: 
```
This is the first paragraph.
Here the second one begins.
```


### New Section
Markup: two newlines resp. blank line

Example:
`Here is the text of first section.

Here a new section begins.`


### Captions and Chapters
Markup: double hash at line beginning

Example: `## Chapter 1`

A line marked this way will be formated as a headline (and interpreted as beginning of a chapter).


### Quotes
Markup: Simple double quote pairs.

Example: `She said: "Oh shit!"`

In output French quotes will be printed.

Refs:

* <https://en.wikipedia.org/wiki/Guillemet>


### Hyphen
Markup: Simple minus.

Example: `man-eating shark`

Copied to HTML and LaTeX code as is.

Refs:

* <http://jakubmarian.com/hyphen-minus-en-dash-and-em-dash-difference-and-usage-in-english/>


### En-dash
Markup: Double minus.

Example: `Glitzer, Filz, Garn und Knöpfe -- seine Küche sah aus als wäre ein Clown in ihr explodiert.`

HTML: will be translated to en-dash (`&ndash;`).
LaTeX: still `--`.

Refs:

* <http://jakubmarian.com/hyphen-minus-en-dash-and-em-dash-difference-and-usage-in-english/>


### Em-dash
Markup: Triple minus.

Example: `Glitter, felt, yarn, and buttons---his kitchen looked as if a clown had exploded.`

HTML: translated to em-dash (`&mdash;`).
LaTeX: still `---`.

Refs:

* <http://jakubmarian.com/hyphen-minus-en-dash-and-em-dash-difference-and-usage-in-english/>


### Italics
Markup: Surround with underscores (_) or asterisks (*).

Example: `_This_ is *italic*.`

The marked text will be printed in italics.

Refs:

* <https://en.wikipedia.org/wiki/Italic_type>


### Bold
Markup: Surround with double underscores (__) or double asterisks (**).

Example: `__This__ is **bold**.`

The marked text will be printed in bold font.

Refs:

* <https://en.wikipedia.org/wiki/Emphasis_(typography)>


### Ellipsis
Markup: Three normal dots.

Example: `That is... ahem... crap.`

HTML: `&hellip;`.
LaTeX: `\dots`.

Refs:

* <https://en.wikipedia.org/wiki/Ellipsis>


### Footnotes
Markup: Square brackets.

Example: `This is the word-after-which-the-footnote-appears[The footnote].`

### Images
Markup: \!\[altText\]\(Image path "optional title"\)

This way images at the given image path can be added. The optional title is used as an image caption. altText is shown in HTML when image cannot be displayed and used by screen readers. In LaTeX altText will be ignored.

Example: `![Alternative text](/path/to/img.jpg "optional title")`

### Masking of Special Characters
Markup: Backslash followed by special character ('`\specialchar`')

In HTML, LaTeX and our markup certain symbols are reserved as parts of the syntax. They have to be masked to be interpreted as the simple symbol and to be shown as such in the generated output text.

The following characters have to be escaped (prepend '`\`'):
`\`   backslash &ndash; the escape character itself  
`*`   asterisk  
`_`   underscore
`{}`  curly brackets
`[]`  square brackets
`#`   hash
`"`   double quote
`!`   exclamation mark (only when followed by '[')  
`--`  double minus  
`$`   dollar

The following characters can be used as normal and will automatically be translated in HTML and LaTeX representation:  
`&`   in HTML becomes '`&amp;`', in LaTeX '`\&`'  
`<`   in HTML becomes `&lt;`  
`>`   in HTML becomes `&gt;`  
`%`   in LaTeX becomes `\%`  
`~`   in LaTeX becomes `\textasciitilde`  
`^`   in LaTeX becomes `\textasciicircum`  

Example: `Angle brackets <very angular> and square brackets \[because it's square\].`

Refs:

* <https://www.w3.org/International/questions/qa-escapes#use>
* <https://en.wikibooks.org/wiki/LaTeX/Basics#Reserved_Characters>

## LaTeX Text Commands

For use in (additional) *LaTeX* documents (see below) or for specific settings (see above) the following commands are provided (in addition to the usual LaTeX commands):

* `\storytitle`     prints the title
* `\storysubtitle`  prints the subtitle
* `\storyhalftitle` prints the half title
* `\storyauthor`    prints the author
* `\storychapter`   prints current chapter
* `\isbn`           prints ISBN
* `\printtitle`     pretty prints title, subtitle (if any) and when *Print author on title* is set also the author

## Templates, Preliminaries, Appendix, License files

## LaTeX License

At file path `latex/license.tex` a license can be provided (as valid LaTeX document). If this file is missing, no license will be appended to the document. 

Directory for license templates: `latex/templates/licenses`

## LaTeX Preliminaries and Appendix

With setup option `bookPrint` set to `true` LaTeX documents for preliminary pages and appendix pages can be put in designated directories. 
Files in those directories will be added to the document in alphanumerical order.

Directory for preliminaries: `latex/bookPreliminaries`

Directory for appendix: `latex/bookAppendix`

Templates can be found in `latex/templates`.

## HTML License

At file path `html/license.tpl.html` a license can be provided (as valid HTML without header and body). The corresponding CSS stylesheet can be found at `html/css/license-styles.css`. 

For license and license CSS templates can be found in `html/templates/licenses`.



