# Copyright (c) 2018 Josa Wode. See LICENSE file for details.

from kivy.config import Config
Config.set('graphics', 'width', '800')
Config.set('graphics', 'height', '600')

from kivy.app import App
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.lang import Observable
from kivy.config import ConfigParser
from kivy.metrics import dp
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.scrollview import ScrollView
from kivy.uix.widget import Widget
from kivy.uix.settings import SettingsWithNoMenu, InterfaceWithNoMenu, SettingsPanel, SettingItem, SettingTitle, SettingBoolean, SettingString, SettingOptions, SettingSpacer
from kivy.uix.tabbedpanel import TabbedPanel, TabbedPanelContent, TabbedPanelItem
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.behaviors import ButtonBehavior  
from kivy.uix.image import Image 
from kivy.uix.dropdown import DropDown
from kivy.uix.switch import Switch
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.checkbox import CheckBox
from kivy.properties import StringProperty, NumericProperty, BooleanProperty, ListProperty, ObjectProperty
from kivy.clock import Clock
from kivy.logger import Logger
import logging

import os
import sys
import copy
from subprocess import Popen, PIPE
from functools import partial

import toml
import json

from localisation import _, change_language_to

from setupdefs import setup_defs

import textstory_converter.filt0r as filt0r

PY2 = sys.version_info.major == 2

configdefaults_general = {
    'title': '',
    'subtitle': '',
    'author': '',
    'language': 'en'#TODO selection field (all languages ?!)
}

configdefaults_latex = {
    'bookPrint': False,
    'headerLeft': '\\storytitle',
    'headerRight': '\\storychapter',
    'hideChapterHeader': True,
    'chapterPagebreak': True,
    'tableOfContents': True,
    'tableOfContentsPagebreak': False,
    'contentsTitle': 'Inhalt',
    'isbn': 'xxx-x-xxxx-xxxx-x',
    'pageSizeByFormat': '1',
    'pageFormat': 'a5',
    'pageWidth': '210mm',
    'pageHeight': '297mm',
    'bindingOffset': '15mm',
    'fontSize': '10',
    'printAuthorOnTitle': False,
    'title': '',
    'subtitle': '',
    'halfTitle': '',
    'pdfsubject': 'Geschichte',
    'pdfkeywords': 'geschichte, alte, josa, wode',
    'hascolorlinks': 'false',
    'urlcolor': 'blue',
    'linkcolor': 'black'
}

configdefaults_html = {
    'title': '',
    'subtitle': '',
    'headertitle': '',
    'url': '',
    'metadescription': '',
    'locale': 'en_GB',#TODO selection field (all locales ?!)
    'sitename': '',
    'previewimage': ''
}

configdefaults = {
    'general': configdefaults_general,
    'latex': configdefaults_latex,
    'html': configdefaults_html
}


configtypes_general = {
    'title': 'str',
    'subtitle': 'str',
    'author': 'str',
    'language': 'str'#TODO lang ?
}

configtypes_latex = {
    'bookPrint': 'bool',
    'headerLeft': 'str',
    'headerRight': 'str',
    'hideChapterHeader': 'bool',
    'chapterPagebreak': 'bool',
    'tableOfContents': 'bool',
    'tableOfContentsPagebreak': 'bool',
    'contentsTitle': 'str',
    'isbn': 'str',
    'pageFormat': 'str',
    'pageWidth': 'str',
    'pageHeight': 'str',
    'bindingOffset': 'str',
    'fontSize': 'str',
    'printAuthorOnTitle': 'bool',
    'title': 'str',
    'subtitle': 'str',
    'halfTitle': 'str',
    'pdfsubject': 'str',
    'pdfkeywords': 'str',
    'hascolorlinks': 'str',
    'urlcolor': 'str',
    'linkcolor': 'str'
}

configtypes_html = {
    'title': 'str',
    'subtitle': 'str',
    'headertitle': 'str',
    'url': 'str',
    'metadescription': 'str',
    'locale': 'str',#TODO locale ?
    'sitename': 'str',
    'previewimage': 'str'
}

configtypes = {
    'general': configtypes_general,
    'latex': configtypes_latex,
    'html': configtypes_html
}


def get_child_by_type(widget, type):
    for child in widget.children:
        if child.__class__.__name__ == type.__name__:
            return child
    return None

class TextstoryCreator(Widget):
    config = ConfigParser()
    language = StringProperty('')
    setup_widgets = set([])
    
    def __init__(self, **kwargs):
        self.config.read('settings.ini')
        self.language = self.config.getdefault('settings', 'language', '')
        self.config.setdefaults('settings', {'language' : 'en'})
        self.language = self.config.get('settings', 'language')
        super(TextstoryCreator, self).__init__(**kwargs)
        self.update_language()
        
    def change_language(self, new_lang):
        self.language = new_lang
        self.config.set('settings', 'language', self.language)
        self.config.write()
        self.update_language()

    def update_language(self):
        change_language_to(self.language)
        self.update_setup_texts()

    def register_for_translation(self, setup_widget):
        self.setup_widgets.add(setup_widget)
        self.update_setup_text(setup_widget)

    def update_setup_texts(self):
        for setup_widget in self.setup_widgets:
            self.update_setup_text(setup_widget)

    def update_setup_text(self, setup_widget):
        #Logger.info(setup_widget.title + ": " + setup_widget.desc)
        setup_widget.title = _(setup_widget.title_ti)
        setup_widget.desc = _(setup_widget.desc_ti)


class LanguageSelection(DropDown):
    pass


class LanguageButton(Button):

    def __init__(self, **kwargs):
        super(LanguageButton, self).__init__(**kwargs)
        self.language_selection = LanguageSelection(auto_width=False)
        
        self.languages = {
            'deutsch': 'de',
            'english': 'en'
        }
        
        for lang_name in sorted(self.languages.keys()):
            btn = Button(text=lang_name, size_hint=[None, None], height=40)
            btn.bind(on_release=lambda btn: self.language_selection.select(btn.text))
            self.language_selection.add_widget(btn)
            self.language_selection.width = max(self.language_selection.width, btn.width + 5)
        
        self.bind(on_release=self.language_selection.open)
        self.language_selection.bind(on_select=lambda instance, x: self.update_language(x))

    def update_language(self, lang_name):
        lang_code = self.languages.get(lang_name)
        self.text = lang_code


class TextstoryPanel(TabbedPanel):
    panel_header_padding = NumericProperty(20)
    panel_header_min_width = NumericProperty(100)

    def __init__(self, **kwargs):
        super(TextstoryPanel, self).__init__(**kwargs)
        self.update_tab_width()

    def update_tab_width(self):
        tab_width = self.panel_header_min_width
        for panel in self.tab_list:
            tab_width = max(panel.texture_size[0], tab_width)
        self.tab_width = tab_width + 2*self.panel_header_padding


class InfoPopup(Popup):
    text = StringProperty('')
    
    def __init__(self, title, text, **kwargs):
        self.title = title
        self.text = text
        super(InfoPopup, self).__init__(**kwargs)


class FilePathPopup(Popup):
    answer = BooleanProperty(False)
    
    def __init__(self, new_path, title, text, **kwargs):
        self.new_path = new_path
        self.title = title
        self.text = text
        super(FilePathPopup, self).__init__(**kwargs)


class CreatePathPopup(Popup):
    answer = BooleanProperty(False)
    hint_text = StringProperty('')

    def __init__(self, title, text, hint_text, **kwargs):
        self.title = title
        self.text = text
        self.hint_text = hint_text
        super(CreatePathPopup, self).__init__(**kwargs)


class PathSelectButton(ButtonBehavior, Image):
    path = StringProperty('')

    def dismiss_popup(self):
        self._popup.dismiss()

    def on_press(self):
        content = FileChooserDialog(select=self.select, cancel=self.dismiss_popup, choose_folder=self.choose_folder)
        self._popup = Popup(title=_("Select path"), content=content, size_hint=(1, 1))
        self._popup.open()

    def select(self, path, filename):
        self.dismiss_popup()
        if filename:
            self.path = filename[0]


class FileChooserDialog(FloatLayout):
    select = ObjectProperty(None)
    cancel = ObjectProperty(None)

    def __init__(self, choose_folder=False, **kwargs):
        super(FileChooserDialog, self).__init__(**kwargs)
        self.ids.filechooser.dirselect = choose_folder
        if choose_folder:
            self.ids.filechooser.filters = [self.is_dir]

    def is_dir(self, directory, filename):
        return os.path.isdir(os.path.join(directory, filename))

    def open_create_file_dialog(self):
        popup_title = _("Create file")
        popup_path = self.ids.filechooser.path
        popup_text = _("Create new file at:\n\n%s\n\n") % (popup_path, )
        popup_hint_text = _("file name")
        if self.ids.filechooser.dirselect:
            popup_title = _("Create folder")
            Logger.info(str(self.ids.filechooser.selection))
            if len(self.ids.filechooser.selection) > 0:
                popup_path = os.path.normpath(self.ids.filechooser.selection[0])
            popup_text = _("Create new folder at:\n\n%s\n\n") % (popup_path, )
            popup_hint_text = _("folder name")
        create_path_popup = CreatePathPopup(popup_title, popup_text, popup_hint_text)
        create_path_popup.bind(on_dismiss=self.on_create_file_answer)
        create_path_popup.open()

    def on_create_file_answer(self, popup):
        if popup.answer == True and popup.path != "":
            Logger.info(": p " + str(self.ids.filechooser.path))
            Logger.info(": s " + str(self.ids.filechooser.selection))
            Logger.info(": n " + str(popup.path))
            selection = [os.path.join(self.ids.filechooser.path, popup.path)]
            if self.ids.filechooser.dirselect:
                if len(self.ids.filechooser.selection) > 0:
                    selection = self.ids.filechooser.selection
                    selection[0] = os.path.join(selection[0], popup.path)
            self.select(self.ids.filechooser.path, selection)


class FileInputField(BoxLayout):
    margin = NumericProperty(10)
    border_width = NumericProperty(2)
    text = StringProperty(_('Drag file here'))
    hint_text = StringProperty(_('or enter file path'))
    path = StringProperty('')
    choose_folder = BooleanProperty(False)
    
    def __init__(self, **kwargs):
        Window.bind(on_dropfile=self.handle_file_drop)
        super(FileInputField, self).__init__(**kwargs)
        
    def handle_file_drop(self, window, file_path):
        app = App.get_running_app()
        if app.root_widget.ids.tabbed_panel.current_tab != app.root_widget.ids.paths_tab:
            return
        if (self.collide_point(window.mouse_pos[0], window.mouse_pos[1])):
            new_path = file_path.decode("utf-8")
            self.update_path(new_path)

    def update_path(self, new_path):
        # no change
        if new_path == self.path:
            return
        # deleted path / set to nothing
        if new_path == "":
            self.path = new_path
            return
        # directory
        if os.path.isdir(new_path):
            self.path = ""
            popup = InfoPopup(_("Invalid file path"), _("Please insert a valid file path.\n\nThis is a directory."))
            popup.open()
            return
        # invalid
        if not os.access(os.path.dirname(new_path), os.W_OK):
            self.path = ""
            Logger.info(self.__class__.__name__ +  ": Bad file path: " + new_path)
            popup = InfoPopup(_("Invalid file path"), _("Please insert a valid file path.\n\nDo you have write access to this path?"))
            popup.open()
            return
        # new file
        if not os.path.isfile(new_path):
            popup = FilePathPopup(new_path, title=_("File does not exist"), text=_("File does not exist. \n\nCreate new file? Path:\n\n") + new_path)
            popup.bind(on_dismiss=self.on_create_file_answer)
            popup.open()
            return
        # ok
        self.path = new_path
        self.on_path_change()
            
    def on_create_file_answer(self, popup):
        if popup.answer == False:
            self.path = ""
            return
        self.path = popup.new_path
        basedir = os.path.dirname(self.path)
        if not os.path.exists(basedir):
            os.makedirs(basedir)
        open(self.path, 'a').close()
        self.on_new_file()
        self.on_path_change()
        popup = InfoPopup(_("Created file"), _("File created at \n\n%s") % os.path.abspath(self.path))
        popup.open()

    def on_new_file(self):
        pass
        
    def on_path_change(self):
        pass


class FolderInputField(FileInputField):
    text = StringProperty(_('Drag folder here'))
    hint_text = StringProperty(_('or enter folder path'))
    choose_folder = BooleanProperty(True)

    def update_path(self, new_path):
        # no change
        if new_path == self.path: 
            return
        # deleted path / set to nothing
        if new_path == "":
            self.path = new_path
            return
        # file
        if os.path.isfile(new_path):
            self.path = ""
            popup = InfoPopup(_("Invalid folder path"), _("Please insert a valid folder path. \n\nThis is a file."))
            popup.open()
            return
        # invalid
        if not os.access(os.path.dirname(new_path), os.W_OK):
            self.path = ""
            Logger.info(self.__class__.__name__ +  ": Bad folder path: " + new_path)
            popup = InfoPopup(_("Invalid folder path"), _("Please insert a valid folder path.\n\nDo you have write access to this path?"))
            popup.open()
            return
        # new directory
        if not os.path.isdir(new_path):
            popup = FilePathPopup(new_path, title=_("Folder does not exist"), text=_("Folder does not exist. \n\nCreate new folder? Path:\n\n") + new_path)
            popup.bind(on_dismiss=self.on_create_file_answer)
            popup.open()
            return
        # ok
        self.path = new_path
        self.on_path_change()

    def on_create_file_answer(self, popup):#actually its folder now
        if popup.answer == False:
            self.path = ""
            return
        self.path = popup.new_path
        os.makedirs(self.path)
        self.on_new_file()
        self.on_path_change()
        popup = InfoPopup(_("Created folder"), _("Folder created at \n\n%s") % os.path.abspath(self.path))
        popup.open()

    def on_new_file(self):#actually its folder now
        pass

    def on_path_change(self):
        pass


class OutputFolderHandler(FolderInputField):
    setup_file_path = StringProperty('')
    story_file_path = StringProperty('')
    
    def on_path_change(self):
        #if default setup file exists in this folder, update
        setup_file_path = os.path.normpath(os.path.join(self.path, 'setup.toml'))
        if os.path.isfile(setup_file_path):
            if self.setup_file_path == "":
                # setup file path was not set before
                self.setup_file_path = setup_file_path
            elif self.setup_file_path != setup_file_path:
                # setup file path is already set, ask wether to change
                setup_popup = FilePathPopup(setup_file_path, title=_("Change setup file path?"), text=_("There is a setup file in this folder. Should the one from output folder be loaded instead of the current file?\n\nCurrent:\n%s\n\nNew:\n%s") % (self.setup_file_path, setup_file_path))
                setup_popup.bind(on_dismiss=self.on_change_setup_file_answer)
                setup_popup.open()
        elif self.setup_file_path == "":
            new_setup_file_popup = FilePathPopup(setup_file_path, title=_("Create default setup file?"), text=_("Do you want to create a new setup file in the output folder?\n\nNew:\n%s") % (setup_file_path, ))
            new_setup_file_popup.bind(on_dismiss=self.on_new_setup_file_answer)
            new_setup_file_popup.open()

        #if default story file exists in this folder, update
        story_file_path = os.path.normpath(os.path.join(self.path, 'textstory.txt'))
        if os.path.isfile(story_file_path):
            if self.story_file_path == "":
                # story file was not set before
                self.story_file_path = story_file_path
            elif self.story_file_path != story_file_path:
                # story file path is already set, ask wether to change
                story_popup = FilePathPopup(story_file_path, title=_("Change story file path?"), text=_("There is a story file in this folder. Should the one from output folder be used instead of the current file?\n\nCurrent:\n%s\n\nNew:\n%s") % (self.story_file_path, story_file_path))
                story_popup.bind(on_dismiss=self.on_change_story_file_answer)
                story_popup.open()
        elif self.story_file_path == "":
            new_story_file_popup = FilePathPopup(story_file_path, title=_("Create default textstory file?"), text=_("Do you want to create a new textstory file in the output folder?\n\nNew:\n%s") % (story_file_path, ))
            new_story_file_popup.bind(on_dismiss=self.on_new_story_file_answer)
            new_story_file_popup.open()

    def on_change_setup_file_answer(self, popup):
        if popup.answer == False:
            return
        self.setup_file_path = popup.new_path

    def on_change_story_file_answer(self, popup):
        if popup.answer == False:
            return
        self.story_file_path = popup.new_path

    def on_new_setup_file_answer(self, popup):
        self.setup_file_in.on_create_file_answer(popup)

    def on_new_story_file_answer(self, popup):
        self.story_file_in.on_create_file_answer(popup)


class SetupFileHandler(FileInputField):
    data = {}
    data_disabled = {}
    
    def __init__(self, **kwargs):
        super(SetupFileHandler, self).__init__(**kwargs)

    """unused
    def get_type(self, category, key):
        if not category in configtypes or not key in configtypes[category]:
            return "unknown"
        return configtypes[category][key]
    """

    def is_type_ok(self, category, key, value):
        if not category in configtypes or not key in configtypes[category]:
            Logger.info(self.__class__.__name__ +  ": Type check: type unknown for: [" + category + "][" + key + "]")
            return "ok"
        type = configtypes[category][key]
        if type == 'str':
            if isinstance(value, basestring if PY2 else str):
                return "ok"
        if type == 'bool':
            if isinstance(value, bool):
                return "ok"
            elif isinstance(value, basestring if PY2 else str):
                if value.lower() == "true" or value.lower() == "false":
                    return "str_to_bool"
            return "Should be " + str(type)
        Logger.warning(self.__class__.__name__ +  ": Type check: invalid type '" + str(type) + "' for: [" + category + "][" + key + "]")
        return "Should be " + str(type)

    def on_new_file(self):
        self.read_from_config()
        self.save()
        
    def on_path_change(self):
        self.load()
            
    def read_from_config(self):
        config = App.get_running_app().config
        for category in configdefaults:
            self.data.update({ category : {} })
            for item in configdefaults[category]:
                self.data[category].update({ item : config.getdefault(category, item, configdefaults[category][item]) })
        #Logger.info(self.__class__.__name__ +  ": Read from config:\n" + str(self.data))
    
    def update_config_category(self, config, settings, category):
        # test if category is in setup data:
        try:
            self.data[category]
        except KeyError:
            Logger.info(self.__class__.__name__ +  ": category " + category + " missing in toml data")
            return
        
        # update items
        for item in self.data[category]:
            value = self.data[category][item]
            config.set(category, item, value)
            settings.update_option_value(category, item, value)
        
    def update_config(self):
        app = App.get_running_app()
        config = app.config
        settings = app.settings
        
        self.update_config_category(config, settings, 'general')
        self.update_config_category(config, settings, 'latex')
        self.update_config_category(config, settings, 'html')
        
        # update states of option self-disabling
        for category in self.data_disabled:
            for item in self.data_disabled[category]:
                app.settings.update_option_disabled(category, item, self.data_disabled[category][item])


    def update_disabled(self, section, key, disabled):
        if not section in self.data_disabled:
            self.data_disabled.update({ section : {} })
        self.data_disabled[section].update({ key : disabled })
        #Logger.info("disabled: " + str(section) + ", " + str(key) + ", " + str(self.data_disabled[section][key]))
        self.save()

    def load(self):
        if not os.path.isfile(self.path):
            Logger.info(self.__class__.__name__ +  ": not a file: " + self.path)
            return
        try:
            setupFileString = filt0r.DocumentReader(self.path).getString()
        except SystemExit as e:
            Logger.info("SetupFileHandler: not a vaild setup file: " + str(e.args[0]))
            self.path = ""
            file_invalid_msg = _("Invalid setup file. Could not decode Utf-8.\n")
            popup = InfoPopup(_("Unicode Decode Error"), file_invalid_msg)
            popup.open()
            return
        try:
            app = App.get_running_app()
            self.read_from_config()
            new_data = toml.loads(setupFileString)
            for category in new_data:
                # update disabled for items from file
                if not category in self.data_disabled:
                    self.data_disabled.update({ category : {} })
                for item in new_data[category]:
                    self.data_disabled[category].update({ item : False })
                    # test types for loaded items
                    new_data_value = new_data[category][item]
                    type_ok = self.is_type_ok(category, item, new_data_value)
                    if type_ok != "ok":
                        # convert string to bool
                        if type_ok == "str_to_bool":
                            new_data[category].update({ item : new_data_value.lower() == "true" })
                            Logger.info(self.__class__.__name__ +  ": type conversion (str to bool): [" + category + "][" + item + "], value: " + str(new_data[category][item]))
                        else:
                            type_invalid_msg = _("Invalid type for [%s][%s].\n%s.") % (category, item, type_ok)
                            popup = InfoPopup(_("Toml Data Type Error"), type_invalid_msg)
                            popup.open()
                            return
                # update config data
                if not category in self.data:
                    self.data[category] = new_data[category]
                else:
                    self.data[category].update(new_data[category])
            # update disabled for items not in file
            for category in self.data:
                for item in self.data[category]:
                    if not category in new_data or not item in new_data[category]:
                        app.settings.update_option_disabled(category, item, True)
            # detect and set "pageSizeByFormat"
            if 'latex' in new_data:
                if 'pageFormat' in new_data['latex']:
                    self.data['latex'].update({ 'pageSizeByFormat' : '1' })
                elif 'pageWidth' in new_data['latex'] and 'pageHeight' in new_data['latex']:
                    self.data['latex'].update({ 'pageSizeByFormat' : '0' })
            
            #Logger.info(self.__class__.__name__ +  ": Loaded :\n" + str(self.data))
            self.save()#values added from config are saved to input file
        except toml.TomlDecodeError:
            Logger.info(self.__class__.__name__ +  ": invalid toml")
            popup = InfoPopup(_("Toml Error"), _("No valid toml file"))
            popup.open()
            return
        self.update_config()
               
    def save_entry(self, section, key, value):
        try:
            self.data[section]
        except:
            self.data.update({ section : {} })
        
        if not key in self.data[section] or self.data[section][key] != value:
            self.data[section].update({ key : value })
            Logger.info(self.__class__.__name__ +  ": saving [" + section + "][" + key + "]: " + value)
            self.save()

    def save(self):
        if self.path != "":
            data = copy.deepcopy(self.data)
            # delete disabled entries
            for section in self.data_disabled:
                for key in self.data_disabled[section]:
                    if self.data_disabled[section][key]:
                        try:
                            del data[section][key]
                        except Exception as e:
                            Logger.info(self.__class__.__name__ +  ": Save error: " + type(e).__name__ + str(e.args) + ",\n data: " + str(data))
            # delete internal options:
            try:
                if 'pageSizeByFormat' in data['latex']:
                    del data['latex']['pageSizeByFormat']
            except Exception as e:
                Logger.info(self.__class__.__name__ +  ": Save error: " + type(e).__name__ + str(e.args) + ",\n data: " + str(data))
            # save
            try:
                setup_toml = toml.dumps(data)
                with open(self.path, "wb") as f:
                    f.write(setup_toml.encode("utf-8"))
            except Exception as e:
                Logger.info(self.__class__.__name__ +  ": Saving failed: " + type(e).__name__ + str(e.args) + ",\n data: " + str(data))


class TextstorySettingsInterface(InterfaceWithNoMenu):
    pass


class TextstorySettings(SettingsWithNoMenu):

    def __init__(self, **kwargs):
        super(TextstorySettings, self).__init__(**kwargs)
        self.register_type('ts_title', TextstorySettingTitle)
        self.register_type('ts_bool', TextstorySettingBoolean)
        self.register_type('ts_string', TextstorySettingString)
        self.register_type('ts_options', TextstorySettingOptions)

    def update_option_disabled(self, section, key, disable):
        setup_widget = self.get_setup_widget(section, key)
        if setup_widget == None:
            Logger.info("Settings: update_option_disabled failed: no widget for [" + section + ", " + key + "] found")
            return
        if setup_widget.optional:
            setup_widget.ids.disable_box.active = not disable
            #Logger.info("Settings: update_option_disabled: [" + section + ", " + key + "] disabled: " + str(disable))
        elif disable:
            #Logger.info("Settings: update_option_disabled: [" + section + ", " + key + "] not optional")
            pass

    def update_option_value(self, section, key, value):
        setup_widget = self.get_setup_widget(section, key)
        setup_widget.value = value

    def get_setup_widget(self, section, key):
        settingsinterface = get_child_by_type(self, TextstorySettingsInterface)
        gridlayout = get_child_by_type(settingsinterface, GridLayout)
        settingsPanel = get_child_by_type(gridlayout, SettingsPanel)
        for item in settingsPanel.children:
            try:
                if item.section == section and item.key == key:
                    return item
            except:
                continue
        return None

    def on_config_change(self, config, section, key, value):
        pass


"""
CheckBox to disable or enable optional setting items
"""
class SettingDisableBox(CheckBox):
    selectable = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(SettingDisableBox, self).__init__(**kwargs)
        self.active = True
        self.available = True

    def on_touch_down(self, touch):
        if not self.collide_point(*touch.pos):
            return
        if not self.available:
            return
        touch.grab(self)
        return super(SettingDisableBox, self).on_touch_down(touch)

    def on_touch_up(self, touch):
        if touch.grab_current is self:
            touch.ungrab(self)
            #toggle active even when disabled, if enabled it toggles anyway
            self.active = not self.active
            return
        return super(SettingDisableBox, self).on_touch_up(touch)


class TextstorySettingItem(Widget):
    optional = BooleanProperty(False)#optional setting items can be disabled
    disabler = ListProperty([])#["section", "key"] of config that toggles disabled
    disabling_value = StringProperty('0')#config value on which disabled is set to true
    hide = NumericProperty(0)
    disabler_value = StringProperty('1')#current value of the disabler

    def __init__(self, **kwargs):
        super(TextstorySettingItem, self).__init__(**kwargs)
        # setup translation identifiers
        self.title_ti = self.title
        self.desc_ti = self.desc
        # register for translation
        app = App.get_running_app()
        app.root_widget.register_for_translation(self)
        # set up disabling from self (for optional setting items)
        self.update_disable_box()
        if self.optional:
            self.ids.disable_box.active = False
        # set up disabling from other setting option
        if self.disabler != []:
            config = app.config
            config.add_callback(self.update_disabled, self.disabler[0], self.disabler[1])
            disabler_value = configdefaults[self.disabler[0]][self.disabler[1]]
            self.update_disabled(self.disabler[0], self.disabler[1], disabler_value)
        app.root_widget.ids.setup_file_input.update_disabled(self.section, self.key, self.disabled)

    def update_disable_box(self):
        if self.optional:
            self.ids.disable_box.selectable = True
        else:
            self.ids.disable_box.available = False
            self.ids.disable_box.disabled = True

    def on_disable_box_change(self):
        self.update_disabled('', '', self.disabler_value)

    def update_disabled(self, section, key, disabler_value):
        #Logger.info('update: [' + self.section + ", " + self.key + "], disabler: [" + section + ", " + key + "], value: " + str(disabler_value))
        #convert bool to '0' or '1'
        if str(disabler_value).lower() == "true":
            disabler_value = '1'
        elif str(disabler_value).lower() == "false":
            disabler_value = '0'
        self.disabler_value = disabler_value
        if disabler_value == self.disabling_value or not self.ids.disable_box.active:
            self.disabled = True
        else:
            self.disabled = False

    def switch_disabled(self):
        if self.disabled:
            self.hide = .5
        else:
            self.hide = 0
        self.update_disable_box()
        app = App.get_running_app()
        app.root_widget.ids.setup_file_input.update_disabled(self.section, self.key, self.disabled)

    """
    put this in subclasses (complicated inheritance):
    def on_touch_down(self, touch):
        if not self.ids.disable_box.on_touch_down(touch)
            return super(TextstorySettingItem, self).on_touch_down(touch)

    def on_touch_up(self, touch):
        if not self.ids.disable_box.on_touch_up(touch)
            return super(TextstorySettingItem, self).on_touch_up(touch)
    """


class TextstorySettingTitle(SettingTitle):
    def __init__(self, **kwargs):
        super(TextstorySettingTitle, self).__init__(**kwargs)
        self.desc = '' # dummy, so it can be handled as setup item for matters of translation
        # setup translation identifiers
        self.title_ti = self.title
        self.desc_ti = self.desc
        # register for translation
        app = App.get_running_app()
        app.root_widget.register_for_translation(self)


class TextstorySettingBoolean(SettingItem, TextstorySettingItem):
    values = ListProperty(['0', '1'])
    active = BooleanProperty(False)
    button_texts = ListProperty([_("No"), _("Yes")])
    
    def __init__(self, **kwargs):
        super(TextstorySettingBoolean, self).__init__(**kwargs)
        self.active = bool(self.values.index(self.value)) if self.value in self.values else False

    def update_value(self):
        self.value = self.values[1] if self.active else self.values[0]

    def update_active(self):
        self.active = True if self.value == self.values[1] else False

    def on_touch_down(self, touch):
        if not self.ids.disable_box.on_touch_down(touch):
            return super(TextstorySettingBoolean, self).on_touch_down(touch)

    def on_touch_up(self, touch):
        if not self.ids.disable_box.on_touch_up(touch):
            if touch.grab_current is self:
                self.active = not self.active
            return super(TextstorySettingBoolean, self).on_touch_up(touch)


class TextstorySettingBooleanToggle(ToggleButton):
    value = BooleanProperty(False)
    down_value = BooleanProperty(False)
    
    def __init__(self, **kwargs):
        super(TextstorySettingBooleanToggle, self).__init__(**kwargs)
        self.update_state()
        
    def update_state(self):
        if self.value == self.down_value:
            self.state = "down"
        else:
            self.state = "normal"
        
    def toggle(self):
        self.value = not self.value


class TextstorySettingString(SettingString, TextstorySettingItem):

    def on_touch_down(self, touch):
        if not self.ids.disable_box.on_touch_down(touch):
            return super(TextstorySettingString, self).on_touch_down(touch)

    def on_touch_up(self, touch):
        if not self.ids.disable_box.on_touch_up(touch):
            return super(TextstorySettingString, self).on_touch_up(touch)


'''
SettingOptions with scrollable options
'''
class TextstorySettingOptions(SettingOptions, TextstorySettingItem):

    def _create_popup(self, instance):
        # create the popup with scrollview
        popup_width = min(0.95 * Window.width, dp(500))
        popup_height = min(len(self.options) * dp(55), Window.height)
        scrollview_height = popup_height -70
        scrollview = ScrollView(size_hint=(1, None), width=popup_width, height=scrollview_height, do_scroll_x=False)
        content = GridLayout(cols=1, spacing='5dp', size_hint_y=None)
        content.bind(minimum_height=content.setter('height'))
        scrollview.add_widget(content)
        self.popup = popup = Popup(
            content=scrollview, title=self.title, size_hint=(None, None),
            size=(popup_width, popup_height))

        # add all the options
        uid = str(self.uid)
        for option in self.options:
            state = 'down' if option == self.value else 'normal'
            btn = ToggleButton(text=option, state=state, group=uid, size_hint_y=None, height=40)
            btn.bind(on_release=self._set_option)
            content.add_widget(btn)

        # finally, add a cancel button to return on the previous panel
        content.add_widget(SettingSpacer())
        btn = Button(text=_('Cancel'), size_hint_y=None, height=40)
        btn.bind(on_release=popup.dismiss)
        content.add_widget(btn)

        # and open the popup !
        popup.open()

    def on_touch_down(self, touch):
        if not self.ids.disable_box.on_touch_down(touch):
            return super(TextstorySettingOptions, self).on_touch_down(touch)

    def on_touch_up(self, touch):
        if not self.ids.disable_box.on_touch_up(touch):
            return super(TextstorySettingOptions, self).on_touch_up(touch)


class LabelLoggingHandler(logging.Handler):

    def __init__(self, label, level=logging.NOTSET):
        logging.Handler.__init__(self, level=level)
        self.label = label

    def emit(self, record):
        "using the Clock module for thread safety with kivy's main loop"
        def f(dt=None):
            self.label.text += "\n" + self.format(record)
        Clock.schedule_once(f)


class ConverterTab(TabbedPanelItem):
    create_pdf = BooleanProperty(False)

    def start_conversion(self, setup_file, input_file, output_folder, logging_label):
        if output_folder == "":
            popup = InfoPopup(_("No Output Folder"), _("Output folder must be set in paths tab before conversion."))
            popup.open()
            return
        if setup_file == "":
            popup = InfoPopup(_("No Setup File"), _("Setup file must be set in paths tab before conversion."))
            popup.open()
            return
        if input_file == "":
            popup = InfoPopup(_("No Textstory File"), _("Textstory file must be set in paths tab before conversion."))
            popup.open()
            return
        Logger.info("ConverterTab: start conversion: " + setup_file + ", " + input_file + ", " + output_folder)
        logging_label.text = ""
        logging_handler = LabelLoggingHandler(logging_label, logging.INFO)
        Logger.addHandler(logging_handler)
        run_successful = True
        try:
            filt0r.run(setup_file, input_file, output_folder)
        except SystemExit as e:
            Logger.info("Converter: " + str(e.args[0]))
            Logger.info("Converter: Abort.")
            run_successful = False
        Logger.removeHandler(logging_handler)
        if self.create_pdf and run_successful:
            #first run
            self.execute_pdf_creation(output_folder, logging_handler, 1)

    def execute_pdf_creation(self, output_folder, logging_handler, exec_count):
        Logger.addHandler(logging_handler)
        Logger.info("")
        if exec_count == 1:
            Logger.info("Creating pdf...")
        else:
            Logger.info("Second run of pdf creation...")
        Logger.info("")
        
        #process output line reader
        def read_output(stdout, *largs):
            line = stdout.readline()
            if line != b'':
                Logger.info(line.decode("utf-8").strip())
                Clock.schedule_once(partial(read_output, stdout))
            else:
                Logger.removeHandler(logging_handler)
                if exec_count < 2:
                    #second run to get page numbers in table of contents right
                    self.execute_pdf_creation(output_folder, logging_handler, exec_count + 1)
        
        #start the process
        latex_path = os.path.join(output_folder, "latex")
        try:
            process = Popen(['lualatex', '--interaction=nonstopmode', '--output-format=pdf', "latex-document.tex"], cwd=latex_path, stdout=PIPE, stderr=PIPE)
        
            #read process output line by line without blocking the app       
            Clock.schedule_once(partial(read_output, process.stdout))
        
        except Exception as e:
            Logger.info("Pdf creation error: " + type(e).__name__ + str(e.args))
            Logger.info("Make sure TeX Live is installed.")


class PdfCreationLabel(Label):
    active = BooleanProperty(False)

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            touch.grab(self)
            return True

    def on_touch_up(self, touch):
        if touch.grab_current is self:
            touch.ungrab(self)
            #toggle pdf creation checkbox
            self.active = not self.active
            return True


class TextstoryCreatorApp(App):
    use_kivy_settings = False
    settings_cls = TextstorySettings
    icon = "/img/app_icon.png"
    
    def display_settings(self, settings):
        '''
        Overriding display_settings to show setting in TabbedPanel.
        Comment from display_settings base:
        Display the settings panel. By default, the panel is drawn directly
        on top of the window. You can define other behaviour by overriding
        this method, such as adding it to a ScreenManager or Popup.
        You should return True if the display is successful, otherwise False.
        :param settings: A :class:`~kivy.uix.settings.Settings`
        instance. You should define how to display it.
        :type config: :class:`~kivy.uix.settings.Settings`
        '''
        if self.root_widget.ids.setup_tab.content is not settings:
            self.root_widget.ids.setup_tab.add_widget(settings)
            self.settings = settings
            return True
        return False

    def build(self):  
        self.title = 'Textstory Creator'
        self.root_widget = TextstoryCreator()
        self.open_settings()

        return self.root_widget

    def on_config_change(self, config, section, key, value):
        #Logger.info(self.__class__.__name__ +  ": config change: " + section + ", " + key + ", " + value)
        self.root_widget.ids.setup_file_input.save_entry(section, key, value)

    '''
    Overriding get_application_config to avoid using ini file
    '''
    def get_application_config(self, defaultpath='%(appdir)s/%(appname)s.ini'):
        return None

    def build_config(self, config):
        config.setdefaults('general', configdefaults_general)
        config.setdefaults('latex', configdefaults_latex)
        config.setdefaults('html', configdefaults_html)

    def build_settings(self, settings):
        settings.add_json_panel('Setup',
                                self.config,
                                data=json.dumps(setup_defs))

    # pausing / resume handlers

    def on_pause(self):
        # Here you can save data if needed
        return True

    def on_resume(self):
        # Here you can check if any data needs replacing (usually nothing)
        pass


if __name__ == '__main__':
    TextstoryCreatorApp().run()