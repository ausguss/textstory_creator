from localisation import _

setup_defs = [
    {
        "type": "ts_title",
        "title": _("General")
    },
    {
        "type": "ts_string",
        "title": _("Title"),
        "desc": _("The title of your story"),
        "section": "general",
        "key": "title"
    },
    {
        "type": "ts_string",
        "title": _("Subtitle"),
        "desc": _("The subtitle of your story if any"),
        "section": "general",
        "key": "subtitle"
    },
    {
        "type": "ts_string",
        "title": _("Author"),
        "desc": _("Name of the storys author(s)"),
        "section": "general",
        "key": "author"
    },
    {
        "type": "ts_string",
        "title": _("Language"),
        "desc": _("The story's language as shortcode (de, en)"),
        "section": "general",
        "key": "language"
    },
    
    
    {
        "type": "ts_title",
        "title": _("LaTeX")
    },
    {
        "type": "ts_title",
        "title": _("LaTeX Title")
    },
    {
        "type": "ts_string",
        "title": _("Title"),
        "desc": _("Title if different from general title"),
        "section": "latex",
        "key": "title",
        "optional": True
    },
    {
        "type": "ts_string",
        "title": _("Subtitle"),
        "desc": _("Subtitle if different from general subtitle"),
        "section": "latex",
        "key": "subtitle",
        "optional": True
    },
    {
        "type": "ts_bool",
        "title": _("Print author on title"),
        "desc": _("Determines if title page begins with author's name"),
        "values": [False, True],
        "section": "latex",
        "key": "printAuthorOnTitle",
        "disabler": ["latex", "bookPrint"],
        "disabling_value": '1',
        "optional": True
    },
    {
        "type": "ts_string",
        "title": _("Half title"),
        "desc": _("Half title if different from title"),
        "section": "latex",
        "key": "halfTitle",
        "optional": True
    },
    {
        "type": "ts_string",
        "title": _("ISBN"),
        "desc": _("The book's ISBN if any"),
        "section": "latex",
        "key": "isbn",
        "optional": True
    },
    {
        "type": "ts_title",
        "title": _("LaTeX Book Print")
    },
    {
        "type": "ts_bool",
        "title": _("Book print"),
        "desc": _("Create document for book printing, allow preliminaries and appendix (directories: latex/bookPreliminaries and latex/bookAppendix)"),
        "values": [False, True],
        "section": "latex",
        "key": "bookPrint"
    },
    {
        "type": "ts_title",
        "title": _("LaTeX Layout")
    },
    {
        "type": "ts_bool",
        "title": _("Page size definition"),
        "desc": _("Select how to specify your page dimensions"),
        "section": "latex",
        "key": "pageSizeByFormat",
        "button_texts": [_("Width/Height"), _("Format")]
    },
    {
        "type": "ts_options",
        "title": _("Page format"),
        "desc": _("Define page size"),
        "options": [
            "a0", 
            "a1", 
            "a2",
            "a3", 
            "a4", 
            "a5",
            "a6",
            "b0", 
            "b1", 
            "b2",
            "b3", 
            "b4", 
            "b5",
            "b6"
        ],
        "section": "latex",
        "key": "pageFormat",
        "disabler": ["latex", "pageSizeByFormat"]
    },
    {
        "type": "ts_string",
        "title": _("Page width"),
        "desc": _("The page's width with unit (e.g. 210mm)"),
        "section": "latex",
        "key": "pageWidth",
        "disabler": ["latex", "pageSizeByFormat"],
        "disabling_value": "1"
    },
    {
        "type": "ts_string",
        "title": _("Page height"),
        "desc": _("The page's height with unit (e.g. 297mm)"),
        "section": "latex",
        "key": "pageHeight",
        "disabler": ["latex", "pageSizeByFormat"],
        "disabling_value": "1"
    },
    {
        "type": "ts_string",
        "title": _("Binding offset"),
        "desc": _("Part of the page that is lost in binding with unit (e.g. 15mm)"),
        "section": "latex",
        "key": "bindingOffset",
        "optional": True
    },
    {
        "type": "ts_string",
        "title": _("Font size"),
        "desc": _("Size of the normal text font in pt (e.g. 11 or 12.5)"),
        "section": "latex",
        "key": "fontSize"
    },
    {
        "type": "ts_title",
        "title": _("LaTeX Header")
    },
    {
        "type": "ts_string",
        "title": _("Header Left"),
        "desc": _("define text for left header, you may use commands \\storytitle, \\storysubtitle, \\storyhalftitle, \\storyauthor, \\storychapter"),
        "section": "latex",
        "key": "headerLeft",
        "optional": True
    },    
    {
        "type": "ts_string",
        "title": _("Header Right"),
        "desc": _("define text for right header, you may use commands \\storytitle, \\storysubtitle, \\storyhalftitle, \\storyauthor, \\storychapter"),
        "section": "latex",
        "key": "headerRight",
        "optional": True
    },
    {
        "type": "ts_title",
        "title": _("LaTeX Table of Contents")
    },
    {
        "type": "ts_bool",
        "title": _("Table of contents"),
        "desc": _("Show table of contents"),
        "values": [False, True],
        "section": "latex",
        "key": "tableOfContents"
    },
    {
        "type": "ts_bool",
        "title": _("Table of contents pagebreak"),
        "desc": _("After table of contents begin on new page"),
        "values": [False, True],
        "section": "latex",
        "key": "tableOfContentsPagebreak"
    },
    {
        "type": "ts_string",
        "title": _("Contents title"),
        "desc": _("Title for table of contents"),
        "section": "latex",
        "key": "contentsTitle"
    },
    {
        "type": "ts_title",
        "title": _("LaTeX Chapter")
    },
    {
        "type": "ts_bool",
        "title": _("Chapter pagebreak"),
        "desc": _("New page when new chapter starts"),
        "values": [False, True],
        "section": "latex",
        "key": "chapterPagebreak"
    },
    {
        "type": "ts_bool",
        "title": _("Hide chapter header"),
        "desc": _("Hide header when new chapter starts"),
        "values": [False, True],
        "section": "latex",
        "key": "hideChapterHeader",
        "optional": True
    },
    {
        "type": "ts_title",
        "title": _("LaTeX PDF Settings")
    },
    {
        "type": "ts_string",
        "title": _("Pdf subject"),
        "desc": _("Subject of the pdf for pdf metadata"),
        "section": "latex",
        "key": "pdfsubject"
    },
    {
        "type": "ts_string",
        "title": _("Pdf keywords"),
        "desc": _("Descriptive keywords for Pdf metadata"),
        "section": "latex",
        "key": "pdfkeywords"
    },
    {
        "type": "ts_bool",
        "title": _("Has color links"),
        "desc": _("Documents hypersetup option colorlinks"),
        "values": ["false", "true"],
        "section": "latex",
        "key": "hascolorlinks",
        "optional": True
    },
    {
        "type": "ts_string",
        "title": _("Url color"),
        "desc": _("Documents hypersetup option urlcolor"),
        "section": "latex",
        "key": "urlcolor",
        "optional": True
    },
    {
        "type": "ts_string",
        "title": _("Link color"),
        "desc": _("Documents hypersetup option linkcolor"),
        "section": "latex",
        "key": "linkcolor",
        "optional": True
    },
    
    
    {
        "type": "ts_title",
        "title": _("Html")
    },
    {
        "type": "ts_title",
        "title": _("Html Title")
    },
    {
        "type": "ts_string",
        "title": _("Title"),
        "desc": _("The title of your story"),
        "section": "html",
        "key": "title",
        "optional": True
    },
    {
        "type": "ts_string",
        "title": _("Subtitle"),
        "desc": _("The subtitle of your story if any"),
        "section": "html",
        "key": "subtitle",
        "optional": True
    },
    {
        "type": "ts_string",
        "title": _("Header title"),
        "desc": _("Title shown in browser tab"),
        "section": "html",
        "key": "headertitle",
        "optional": True
    },
    {
        "type": "ts_title",
        "title": _("Html Metadata")
    },
    {
        "type": "ts_string",
        "title": _("Meta description"),
        "desc": _("A short description for HTML metadata (description and og:description tags)"),
        "section": "html",
        "key": "metadescription"
    },
    {
        "type": "ts_string",
        "title": _("Locale"),
        "desc": _("The text's locale (e.g. de_DE or en_GB) for HTML metadata (og:locale tag)"),
        "section": "html",
        "key": "locale"
    },
    {
        "type": "ts_string",
        "title": _("URL"),
        "desc": _("\"Link\" to the (html) story (og:url tag)"),
        "section": "html",
        "key": "url"
    },
    {
        "type": "ts_string",
        "title": _("Site name"),
        "desc": _("Name of the website hosting your text (og:site_name tag)"),
        "section": "html",
        "key": "sitename"
    },
    {
        "type": "ts_string",
        "title": _("Preview image"),
        "desc": _("Image shown when your text is posted somewhere else (og:image tag)"),
        "section": "html",
        "key": "previewimage",
        "optional": True
    }
]