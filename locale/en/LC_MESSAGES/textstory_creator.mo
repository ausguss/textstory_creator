��    �      �  �   �	      �  '   �  K     )   M     w     ~  
   �     �     �     �     �     �     �     �               +  �   J     �     �  
   �     �          '     4     C  %   T  2   z  E   �  I   �  &   =  %   d  $   �     �     �     �     �     �  	             0  /   D     t     �  3   �  	   �     �     �  
   �  "   �          ,     8     E     R  #   f     �     �  
   �     �  	   �  B   �     �       ,         M     l     r     �     �     �     �     �     �     �  	   �  
   �  	         
          "  8   ?      x     �     �     �     �     �     �  9   �          )     5  
   J  >   U     �     �     �  H   �  6     J   ;  4   �     �     �     �  *   �       	     6   '     ^  	   u  4        �  #   �     �  +   �          0     L  :   \     �  (   �  '   �  *   �  !   *  I   L     �  �   �  �   3     �     �  %   �     �       
   .     9     =  	   R     \     i  
   m  {   x  |   �  	   q     {  	   �     �     �     �     �     �  q    '   w  K   �  )   �            
   +     6     =     U     m          �     �     �     �     �  �   �     j      v   
   �      �      �      �      �      �   %   �   2   !  E   K!  I   �!  &   �!  %   "  $   ("     M"     \"     m"     �"     �"  	   �"     �"     �"  /   �"     #     )#  3   ?#  	   s#     }#     �#  
   �#  "   �#     �#     �#     �#     �#     �#  #   $     ($     -$  
   ;$     F$  	   K$  B   U$     �$     �$  ,   �$     �$     
%     %     !%     /%     <%     I%     \%     t%     �%  	   �%  
   �%  	   �%     �%     �%     �%  8   �%      &     7&     :&     K&     Y&     k&     ~&  9   �&     �&     �&     �&  
   �&  >   �&     2'     @'     M'  H   Y'  6   �'  J   �'  4   $(     Y(     g(     }(  *   �(     �(  	   �(  6   �(     �(  	   )  4   )     R)  #   c)     �)  +   �)     �)     �)     �)  :   �)     5*  (   L*  '   u*  *   �*  !   �*  I   �*     4+  �   L+  �   �+     T,     Z,  %   v,     �,     �,  
   �,     �,     �,  	   �,     �,     -  
   -  {   -  |   �-  	   .     .  	   %.     /.     B.     W.     q.     �.     =   3       \          2                  M   �   0       �             �       .   x      8              k   �   g   �                   <   H   �   j   s   �   9   b   p          �       �   �   D   1   /       {               '   m   �   r   U   !   �      c       *   �      %   �               6          W       Z   N   `   ~   J   S   P       o   #   T      �   +                    }          &   I   �       [      ^          �      i       $   F   )             �   A              G   Y   d   e                  ;       Q   7   B       K   z   �   
   �   C   �          t   ,       w   h       E   �       ?   f           �   -   v      y                      l       "   _       4   n   |   R   �             L   u   X       ]       a   (   5          O           V   	   q   :          @   >        "Link" to the (html) story (og:url tag) A short description for HTML metadata (description and og:description tags) After table of contents begin on new page Author Binding offset Book print Cancel Change setup file path? Change story file path? Chapter pagebreak Computer Console Output Contents title Convert Create default setup file? Create default textstory file? Create document for book printing, allow preliminaries and appendix (directories: latex/bookPreliminaries and latex/bookAppendix) Create file Create folder Create new Create new file at:

%s

 Create new folder at:

%s

 Created file Created folder Define page size Descriptive keywords for Pdf metadata Determines if title page begins with author's name Do you want to create a new setup file in the output folder?

New:
%s Do you want to create a new textstory file in the output folder?

New:
%s Documents hypersetup option colorlinks Documents hypersetup option linkcolor Documents hypersetup option urlcolor Drag file here Drag folder here Drag output folder here Drag setup file here Drag story file here Favorites File created at 

%s File does not exist File does not exist. 

Create new file? Path:

 Folder created at 

%s Folder does not exist Folder does not exist. 

Create new folder? Path:

 Font size Format General Half title Half title if different from title Has color links Header Left Header Right Header title Hide chapter header Hide header when new chapter starts Html Html Metadata Html Title ISBN Icon View Image shown when your text is posted somewhere else (og:image tag) Invalid file path Invalid folder path Invalid setup file. Could not decode Utf-8.
 Invalid type for [%s][%s].
%s. LaTeX LaTeX Book Print LaTeX Chapter LaTeX Header LaTeX Layout LaTeX PDF Settings LaTeX Table of Contents LaTeX Title Language Libraries Link color List View Locale Meta description Name of the storys author(s) Name of the website hosting your text (og:site_name tag) New page when new chapter starts No No Output Folder No Setup File No Textstory File No valid toml file Ok Output folder must be set in paths tab before conversion. Page format Page height Page size definition Page width Part of the page that is lost in binding with unit (e.g. 15mm) Path settings Pdf keywords Pdf subject Please insert a valid file path.

Do you have write access to this path? Please insert a valid file path.

This is a directory. Please insert a valid folder path.

Do you have write access to this path? Please insert a valid folder path. 

This is a file. Preview image Print author on title Select Select how to specify your page dimensions Select path Set Paths Setup file must be set in paths tab before conversion. Show table of contents Site name Size of the normal text font in pt (e.g. 11 or 12.5) Start conversion Subject of the pdf for pdf metadata Subtitle Subtitle if different from general subtitle Table of contents Table of contents pagebreak Textstory Setup Textstory file must be set in paths tab before conversion. The book's ISBN if any The page's height with unit (e.g. 297mm) The page's width with unit (e.g. 210mm) The story's language as shortcode (de, en) The subtitle of your story if any The text's locale (e.g. de_DE or en_GB) for HTML metadata (og:locale tag) The title of your story There is a setup file in this folder. Should the one from output folder be loaded instead of the current file?

Current:
%s

New:
%s There is a story file in this folder. Should the one from output folder be used instead of the current file?

Current:
%s

New:
%s Title Title for table of contents Title if different from general title Title shown in browser tab Toml Data Type Error Toml Error URL Unicode Decode Error Url color Width/Height Yes create pdf define text for left header, you may use commands \storytitle, \storysubtitle, \storyhalftitle, \storyauthor, \storychapter define text for right header, you may use commands \storytitle, \storysubtitle, \storyhalftitle, \storyauthor, \storychapter file name folder name language: or enter file path or enter folder path or enter output file path or enter setup file path or enter story file path Project-Id-Version: 
POT-Creation-Date: 2018-05-13 23:44+0200
PO-Revision-Date: 2018-05-13 23:46+0200
Last-Translator: 
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
X-Generator: Poedit 2.0.7
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
 "Link" to the (html) story (og:url tag) A short description for HTML metadata (description and og:description tags) After table of contents begin on new page Author Binding offset Book print Cancel Change setup file path? Change story file path? Chapter pagebreak Computer Console Output Contents title Convert Create default setup file? Create default textstory file? Create document for book printing, allow preliminaries and appendix (directories: latex/bookPreliminaries and latex/bookAppendix) Create file Create folder Create new Create new file at:

%s

 Create new folder at:

%s

 Created file Created folder Define page size Descriptive keywords for Pdf metadata Determines if title page begins with author's name Do you want to create a new setup file in the output folder?

New:
%s Do you want to create a new textstory file in the output folder?

New:
%s Documents hypersetup option colorlinks Documents hypersetup option linkcolor Documents hypersetup option urlcolor Drag file here Drag folder here Drag output folder here Drag setup file here Drag story file here Favorites File created at 

%s File does not exist File does not exist. 

Create new file? Path:

 Folder created at 

%s Folder does not exist Folder does not exist. 

Create new folder? Path:

 Font size Format General Half title Half title if different from title Has color links Header Left Header Right Header title Hide chapter header Hide header when new chapter starts Html Html Metadata Html Title ISBN Icon View Image shown when your text is posted somewhere else (og:image tag) Invalid file path Invalid folder path Invalid setup file. Could not decode Utf-8.
 Invalid type for [%s][%s].
%s. LaTeX LaTeX Book Print LaTeX Chapter LaTeX Header LaTeX Layout LaTeX PDF Settings LaTeX Table of Contents LaTeX Title Language Libraries Link color List View Locale Meta description Name of the storys author(s) Name of the website hosting your text (og:site_name tag) New page when new chapter starts No No Output Folder No Setup File No Textstory File No valid toml file Ok Output folder must be set in paths tab before conversion. Page format Page height Page size definition Page width Part of the page that is lost in binding with unit (e.g. 15mm) Path settings Pdf keywords Pdf subject Please insert a valid file path.

Do you have write access to this path? Please insert a valid file path.

This is a directory. Please insert a valid folder path.

Do you have write access to this path? Please insert a valid folder path. 

This is a file. Preview image Print author on title Select Select how to specify your page dimensions Select path Set Paths Setup file must be set in paths tab before conversion. Show table of contents Site name Size of the normal text font in pt (e.g. 11 or 12.5) Start conversion Subject of the pdf for pdf metadata Subtitle Subtitle if different from general subtitle Table of contents Table of contents pagebreak Textstory Setup Textstory file must be set in paths tab before conversion. The book's ISBN if any The page's height with unit (e.g. 297mm) The page's width with unit (e.g. 210mm) The story's language as shortcode (de, en) The subtitle of your story if any The text's locale (e.g. de_DE or en_GB) for HTML metadata (og:locale tag) The title of your story There is a setup file in this folder. Should the one from output folder be loaded instead of the current file?

Current:
%s

New:
%s There is a story file in this folder. Should the one from output folder be used instead of the current file?

Current:
%s

New:
%s Title Title for table of contents Title if different from general title Title shown in browser tab Toml Data Type Error Toml Error URL Unicode Decode Error Url color Width/Height Yes create pdf define text for left header, you may use commands \storytitle, \storysubtitle, \storyhalftitle, \storyauthor, \storychapter define text for right header, you may use commands \storytitle, \storysubtitle, \storyhalftitle, \storyauthor, \storychapter file name folder name language: or enter file path or enter folder path or enter output file path or enter setup file path or enter story file path 