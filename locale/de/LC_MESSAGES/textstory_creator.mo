��    �      �  �   �	      �  '   �  K     )   M     w     ~  
   �     �     �     �     �     �     �     �               +  �   J     �     �  
   �     �          '     4     C  %   T  2   z  E   �  I   �  &   =  %   d  $   �     �     �     �     �     �  	             0  /   D     t     �  3   �  	   �     �     �  
   �  "   �          ,     8     E     R  #   f     �     �  
   �     �  	   �  B   �     �       ,         M     l     r     �     �     �     �     �     �     �  	   �  
   �  	         
          "  8   ?      x     �     �     �     �     �     �  9   �          )     5  
   J  >   U     �     �     �  H   �  6     J   ;  4   �     �     �     �  *   �       	     6   '     ^  	   u  4        �  #   �     �  +   �          0     L  :   \     �  (   �  '   �  *   �  !   *  I   L     �  �   �  �   3     �     �  %   �     �       
   .     9     =  	   R     \     i  
   m  {   x  |   �  	   q     {  	   �     �     �     �     �     �  q    )   w  V   �  ?   �     8     A  	   M     W     _     |     �     �     �     �     �     �  !      ~   1      �      �      �      �   %   !     (!     7!     L!  4   h!  B   �!  7   �!  M   "  0   f"  /   �"  .   �"     �"  #   #  *   8#  "   c#  "   �#  	   �#     �#     �#  6   �#     $     /$  @   K$     �$     �$  	   �$     �$  (   �$     �$     �$     %     %     "%  >   >%     }%     �%  
   �%     �%     �%  _   �%     &     %&  9   A&  "   {&     �&     �&     �&     �&     �&     �&     �&     '     '     $'  	   1'     ;'     I'     X'  "   i'  C   �'      �'     �'     �'     (     (     4(     Q(  X   T(     �(     �(     �(     �(  F   �(     7)     I)  	   ])  P   g)  C   �)  V   �)  D   S*     �*     �*  
   �*  +   �*     �*     �*  P   
+  )   [+  	   �+  ;   �+     �+  $   �+  
   ,  =   ,     O,      b,     �,  T   �,  %   �,  ,   -  -   C-  /   q-  .   �-  `   �-     1.  �   J.  �   �.     `/     f/  3   �/  #   �/     �/     �/     �/     0  	   0     '0     40     70  �   E0  �   �0  	   �1     �1     �1     �1     �1      �1      2      "2     =   3       \          2                  M   �   0       �             �       .   x      8              k   �   g   �                   <   H   �   j   s   �   9   b   p          �       �   �   D   1   /       {               '   m   �   r   U   !   �      c       *   �      %   �               6          W       Z   N   `   ~   J   S   P       o   #   T      �   +                    }          &   I   �       [      ^          �      i       $   F   )             �   A              G   Y   d   e                  ;       Q   7   B       K   z   �   
   �   C   �          t   ,       w   h       E   �       ?   f           �   -   v      y                      l       "   _       4   n   |   R   �             L   u   X       ]       a   (   5          O           V   	   q   :          @   >        "Link" to the (html) story (og:url tag) A short description for HTML metadata (description and og:description tags) After table of contents begin on new page Author Binding offset Book print Cancel Change setup file path? Change story file path? Chapter pagebreak Computer Console Output Contents title Convert Create default setup file? Create default textstory file? Create document for book printing, allow preliminaries and appendix (directories: latex/bookPreliminaries and latex/bookAppendix) Create file Create folder Create new Create new file at:

%s

 Create new folder at:

%s

 Created file Created folder Define page size Descriptive keywords for Pdf metadata Determines if title page begins with author's name Do you want to create a new setup file in the output folder?

New:
%s Do you want to create a new textstory file in the output folder?

New:
%s Documents hypersetup option colorlinks Documents hypersetup option linkcolor Documents hypersetup option urlcolor Drag file here Drag folder here Drag output folder here Drag setup file here Drag story file here Favorites File created at 

%s File does not exist File does not exist. 

Create new file? Path:

 Folder created at 

%s Folder does not exist Folder does not exist. 

Create new folder? Path:

 Font size Format General Half title Half title if different from title Has color links Header Left Header Right Header title Hide chapter header Hide header when new chapter starts Html Html Metadata Html Title ISBN Icon View Image shown when your text is posted somewhere else (og:image tag) Invalid file path Invalid folder path Invalid setup file. Could not decode Utf-8.
 Invalid type for [%s][%s].
%s. LaTeX LaTeX Book Print LaTeX Chapter LaTeX Header LaTeX Layout LaTeX PDF Settings LaTeX Table of Contents LaTeX Title Language Libraries Link color List View Locale Meta description Name of the storys author(s) Name of the website hosting your text (og:site_name tag) New page when new chapter starts No No Output Folder No Setup File No Textstory File No valid toml file Ok Output folder must be set in paths tab before conversion. Page format Page height Page size definition Page width Part of the page that is lost in binding with unit (e.g. 15mm) Path settings Pdf keywords Pdf subject Please insert a valid file path.

Do you have write access to this path? Please insert a valid file path.

This is a directory. Please insert a valid folder path.

Do you have write access to this path? Please insert a valid folder path. 

This is a file. Preview image Print author on title Select Select how to specify your page dimensions Select path Set Paths Setup file must be set in paths tab before conversion. Show table of contents Site name Size of the normal text font in pt (e.g. 11 or 12.5) Start conversion Subject of the pdf for pdf metadata Subtitle Subtitle if different from general subtitle Table of contents Table of contents pagebreak Textstory Setup Textstory file must be set in paths tab before conversion. The book's ISBN if any The page's height with unit (e.g. 297mm) The page's width with unit (e.g. 210mm) The story's language as shortcode (de, en) The subtitle of your story if any The text's locale (e.g. de_DE or en_GB) for HTML metadata (og:locale tag) The title of your story There is a setup file in this folder. Should the one from output folder be loaded instead of the current file?

Current:
%s

New:
%s There is a story file in this folder. Should the one from output folder be used instead of the current file?

Current:
%s

New:
%s Title Title for table of contents Title if different from general title Title shown in browser tab Toml Data Type Error Toml Error URL Unicode Decode Error Url color Width/Height Yes create pdf define text for left header, you may use commands \storytitle, \storysubtitle, \storyhalftitle, \storyauthor, \storychapter define text for right header, you may use commands \storytitle, \storysubtitle, \storyhalftitle, \storyauthor, \storychapter file name folder name language: or enter file path or enter folder path or enter output file path or enter setup file path or enter story file path Project-Id-Version: 
POT-Creation-Date: 2018-05-13 23:44+0200
PO-Revision-Date: 2018-05-13 23:53+0200
Last-Translator: 
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
X-Generator: Poedit 2.0.7
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
 "Link" zur (html-)Geschichte (og:url-Tag) Eine kurze Beschreibung für die HTML-Metadaten (description- und og:description-Tags) Nach dem Inhaltsverzeichnis wird auf einer neuen Seite begonnen Autor*in Bundversatz Buchdruck Abbruch Pfad zu Setupdatei anpassen? Pfad zu Storydatei anpassen? Kapitel Seitenumbruch Computer Konsolenausgabe Titel des Inhaltsverzeichnis Konvertieren Standard Setupdatei anlegen? Standard Textstory-Datei anlegen? Dokument für Buchdruck erstellen, Titelei und Anhang erlauben (Verzeichnisse: latex/bookPreliminaries und latex/bookAppendix) Datei erstellen Verzeichnis erstellen Neu erstellen Erstelle neue Datei in:

%s

 Neues Verzeichnis erstellen in:

%s

 Datei erstellt Verzeichnis erstellt Definiere die Seitengröße Beschreibende Schlüsselworte für die Pdf-Metadaten Bestimmt, ob die Titelseite mit dem Namen des*der Autor*in beginnt Neue Setupdatei in Ausgabeverzeichnis anlegen?

Neu:
%s Soll eine neue Textstorydatei im Ausgabeverzeichnis angelegt werden?

Neu:
%s Die Hypersetup-Option 'colorlinks' des Dokuments Die Hypersetup-Option 'linkcolor' des Dokuments Die Hypersetup-Option 'urlcolor' des Dokuments Datei per Drag&Drop festlegen Verzeichnis per Drag&Drop festlegen Ausgabeverzeichnis per Drag&Drop festlegen Setupdatei per Drag&Drop festlegen Storydatei per Drag&Drop festlegen Favoriten Datei erstellt: 

%s Datei existiert nicht Datei existiert nicht. 

Neue Datei erstellen? Pfad:

 Verzeichnis erstellt:

%s Verzeichnis existiert nicht Verzeichnis existiert nicht.

Neues Verzeichnis anlegen? Pfad:

 Schriftgröße Format Allgemein Schmutztitel Schmutztitel, falls abweichend vom Titel Hat farbige Links Kopfzeile links Kopfzeile rechts Header-Titel Kapitelkopfzeile verstecken Keine Kopfzeile auf Seiten auf denen ein neues Kapitel beginnt HTML HTML Metadaten HTML Titel ISBN Symbolansicht Bild, das gezeigt wird, wenn die (Html-)Geschichte irgendwo anders geposted wird (og:image-Tag) Ungültiger Dateipfad Ungültiger Verzeichnispfad Ungültige Setupdatei. Utf-8-Dekodierung fehlgeschlagen.
 Ungültiger Typ für [%s][%s].
%s. LaTeX LaTeX Buchdruck LaTeX Kapitel LaTeX Kopfzeile LaTeX Layout LaTeX PDF-Einstellungen LaTeX Inhaltsverzeichnis LaTeX Titel Sprache Bibliotheken Linkfarbe Listenansicht Regionalangabe Metabeschreibung Autorenname(n) für die Geschichte Name der Webseite auf der der Text gehosted wird (og:site_name-Tag) Kapitel beginnen auf neuer Seite Nein Kein Ausgabeverzeichnis Keine Setupdatei Keine Textstorydatei Kein gültiges Toml-Dokument Ok Ausgabeverzeichnis muss vor der Konvertierung in den Pfadeinstellungen angegeben werden. Seitenformat Seitenhöhe Definition der Seitengröße Seitenbreite Teil der Seite der in der Bindung verschwindet mit Einheit (z.B. 15mm) Pfadeinstellungen Pdf-Schlüsselworte Pdf-Thema Bitte einen gültigen Dateipfad angeben.

Besteht Schreibzugriff zu diesem Pfad? Bitte einen gültigen Dateipfad angeben.

Dies ist ein Verzeichnis. Bitte einen gültigen Verzeichnispfad angeben.

Besteht Schreibzugriff zu diesem Pfad? Bitte einen gültigen Verzeichnispfad angeben.

Dies ist eine Datei. Vorschaubild Autor*in im Titel Auswählen Wähle wie die Seitengröße angegeben wird Pfadauswahl Pfade festlegen Setupdatei muss vor der Konvertierung in den Pfadeinstellungen angegeben werden. Es wird ein Inhaltsverzeichnis eingefügt Site-Name Schriftgröße für normalen Text in pt (z.B. 11 oder 12.5) Konvertierung starten Thema des Pdf für die Pdf-Metadaten Untertitel Untertitel, falls abweichend vom Untertitel unter 'Allgemein' Inhaltsverzeichnis Inhaltsverzeichnis Seitenumbruch Textstory Einstellungen Textstorydatei muss vor der Konvertierung in den Pfadeinstellungen angegeben werden. Die ISBN des Buches (falls vorhanden) Die Höhe der Seite mit Einheit (z.B. 297mm) Die Breite der Seite mit Einheit (z.B. 210mm) Die Sprache der Geschichte als Kürzel (de, en) Der Untertitel der Geschichte, falls vorhanden Die Regionalangabe für den Text (z.B. de_DE oder en_GB) für die HTML-Metadaten (og:locale-Tag) Der Titel der Geschichte Es wurde eine Setupdatei im Ausgabeverzeichnis gefunden. Soll diese geladen und die aktuelle Datei verworfen werden?

Aktuell:
%s

Neu:
%s Es wurde eine Storydatei im Ausgabeverzeichnis gefunden. Soll diese geladen und die aktuelle Datei verworfen werden?

Aktuell:
%s

Neu:
%s Titel Titel des Inhaltsverzeichnisses Titel, falls abweichend vom Titel unter 'Allgemein' Titel für die Kopfzeile im Browser Toml Datentypfehler Toml Fehler URL Unicode Dekodierungsfehler Url-Farbe Breite/Höhe Ja pdf erstellen Definiere den Text der linken Kopfzeile, es können folgende Befehle verwendet werden: \storytitle, \storysubtitle, \storyhalftitle, \storyauthor, \storychapter Definiere den Text der rechten Kopfzeile, es können folgende Befehle verwendet werden: \storytitle, \storysubtitle, \storyhalftitle, \storyauthor, \storychapter Dateiname Verzeichnisname Sprache: oder Dateipfad eingeben oder Verzeichnispfad angeben oder Ausgabeverzeichnis eingeben oder Pfad zu Setupdatei eingeben oder Pfad zu Storydatei eingeben 